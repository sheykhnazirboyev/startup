import React from "react";
import { reduxForm } from "redux-form";
import EditorField from "./EditorField";

const handleForm = values => {
  alert(`Awesome editor text value is ${values.editorText}`);
};
const HelloForm = props => {

  const { handleSubmit } = props;

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <EditorField
        key="field"
        name="editorText"
        id="inputEditorText"
        disabled={false}
        placeholder="Type here"
      />
      <button key="submit" type="submit">
        Submit
      </button>
    </form>
  );
};


function validate(values){
  const errors = {}

  if(!values.editorText)
  {
    errors.editorText = "Editor required"
  }

  return errors;

}


export default reduxForm({
   validate,
  form: "draft"
})(HelloForm);
