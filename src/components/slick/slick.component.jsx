import React, {Component } from 'react';
import "./slick.styles.css";

import Slider from 'react-slick';

	function SampleNextArrow(props) {
	  const { className, style, onClick } = props;
	  return (
	    <div
	      className={className}
	      style={{ ...style, display: "block", background: "#aaa" }}
	      onClick={onClick}
	    />
	  );
	}

		function SamplePrevArrow(props) {
	  const { className, style, onClick } = props;
	  return (
	    <div
	      className={className}
	      style={{ ...style, display: "block", background: "#aaa" }}
	      onClick={onClick}
	    />
	  );
	}

class slick extends Component{



	render()
	{
		const settings = {
	      dots: false,
	      infinite: true,
	      slidesToShow: 3,
	      slidesToScroll: 1,
	      nextArrow: <SampleNextArrow />,
	      prevArrow: <SamplePrevArrow />
	    };
		return(
			
			<div className = "slick-container">
				<Slider {...settings} >
					<div>Slide 1</div>
					<div>Slide 2</div>
					<div>Slide 3</div>
					<div>Slide 4</div>
					<div>Slide 5</div>
					<div>Slide 6</div>
					<div>Slide 7</div>
				</Slider>		
			</div>
			
			);
	}
}

export default slick;