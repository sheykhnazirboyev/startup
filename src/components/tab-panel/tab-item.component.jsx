import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TabItem extends Component {
  static propTypes = {
    activeTab: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  onClick = () => {
    const { label, onClick } = this.props;
    onClick(label);
  }

  render() {
    const {
      onClick,
      props: {
        activeTab,
        label,
        title,
        subtitle,
        icon
      },
    } = this;

    if(this.props.styled !== "personal")
    {
        let myClass = 'tab-list-item';

        if (activeTab === label) {
          myClass += ' tab-list-active';
        }
      
        return (
          <span
            className={myClass}
            onClick={onClick} 
          >{title}</span>
        );

    } else {
      
        let myClass = 'tab-item';

        if (activeTab === label) {
          myClass += ' active-tab-item';
          
          
        }
    return(
        <div className={myClass}  onClick={this.onClick} >
                <div className="tab-text">
                    <h6>{title}</h6>
                    <p>{subtitle}</p>
                </div>
                <div className="tab-icon">
                    <i className={icon}></i>
                </div>
          </div>)
    }

  }
}

export default TabItem;