import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TabItem from './tab-item.component';

import "./tab-panel.styles.css";


class TabPanel extends Component{
    static propTypes = {
        children: PropTypes.instanceOf(Array).isRequired,
      }
    
      constructor(props) {
        super(props);
    
        this.state = {
          activeTab: this.props.children[0].props.label,
        };

        this.changeTab = this.changeTab.bind(this);
        
      }
      componentDidUpdate(prevProps, prevState){
        if(prevProps.onActiveTab !== this.props.onActiveTab){
          this.changeTab();
        }
      }
      changeTab(){
          this.onClickTabItem(this.props.activeLabel);
      }
        
      onClickTabItem = (tab) => {
        this.setState({ activeTab: tab });
      }
    
      render() {
        const {
          onClickTabItem,
          props: {
            children,
          },
          state: {
            activeTab,
          }
        } = this;
      
    return (
        <div>
         {this.props.styled !==  "personal" ?  
            <div className="tab-panel-root">
                <div className="tab-panel-container">
                    <nav className="tab-panel-items">
                    {children.map((child) => {
                const { label, title } = child.props;
                
                return (
                  <TabItem
                    activeTab={activeTab}
                    key={label}
                    label={label}
                    title = {title}
                    onClick={onClickTabItem}
                  />
                );
              })}
                    </nav>
                </div>
                
            </div>
            : 
            <div className = "personal-projects-tab-root">
              <div className  ="personal-projects-tab-container">
                  {children.map((child) => {
                  const { label, title, subtitle, icon } = child.props;
                  
                  return (
                    
                      
                        <TabItem
                          styled = "personal"
                          activeTab={activeTab}
                          key={label}
                          label={label}
                          title = {title}
                          subtitle = {subtitle}
                          icon = {icon}
                          onClick={onClickTabItem}
                        />
                        
                    
                   
                  );
                })}
              </div>
            </div>
          }   
        <div className="tab-content">
        {children.map((child) => {
            if (child.props.label !== activeTab) return undefined;
            return child.props.children;
        })}
    </div>
        </div>
    )
}
}

export default TabPanel;
