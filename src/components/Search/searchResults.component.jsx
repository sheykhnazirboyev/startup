import React, {Component} from 'react';
import {connect} from 'react-redux';
import "./searchResults.styles.css";
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {SearchResultAction, SearchQuery} from '../../redux/project/project.actions';

import Category  from '../individual-category/individualCategory.component';


class SearchResult extends Component
{
	componentDidMount()
	{
		if(this.props.searchQuery && this.props.searchQuery.length > 0)
		{
			
				const url = "http://startupmarket.uz/api/projects"
				const filter = {filter: {
					"where":{ "title":{"ilike":`%${this.props.searchQuery}%`}},
					"include" : ["category", "user"]
				}}
				

				axios.get(url, {params: filter})
				.then(response => this.props.SearchResultAction(response.data))
				.catch(error => console.log(error))
			}
			
	}
	
	componentWillReceiveProps(newProps)
	{
		console.log(newProps)
		if(newProps.searchQuery !== this.props.searchQuery)
		{
			if(newProps.searchQuery && newProps.searchQuery.length > 0)
			{
				const url = "http://startupmarket.uz/api/projects"
				const filter = {filter: {
					"where":{ "title":{"ilike":`%${newProps.searchQuery}%`}},
					"include" : ["category", "user"]
				}}
				
				axios.get(url, {params: filter})
				.then(response => this.props.SearchResultAction(response.data))
				.catch(error => console.log(error))
			}	
		}
	}

	componentWillUnmount()
	{
		this.props.SearchQuery("");
	}
	

	render(){
		const {searchResult, findResult} = this.props;

		let result = "";
		if(searchResult && searchResult.length > 0)
		{
			result = <Category projects = {searchResult} category ="Result"/>
		} else if(findResult){
			result = <div className = "text-center">
						<h2 className = "text-muted">Not found</h2>
					</div>
		}

		return(
			<div className = "search-root">
				<div className = "search-container">
					{result}
				</div>
			</div>
			)
	}
}

const mapStateToProps = state => ({
	searchQuery: state.projects.searchQuery,
	searchResult: state.projects.searchResult,
	lang: state.auth.lang,
	findResult: state.projects.findResult,
	
})
const mapDispatchToProps = dispatch => (
	bindActionCreators({SearchResultAction, SearchQuery}, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);