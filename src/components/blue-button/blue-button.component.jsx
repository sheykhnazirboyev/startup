import React from 'react';
import {NavLink} from 'react-router-dom';
import "./blue-button.styles.css";

 function BlueButton({text}) {
    return (
        <div className="blue-button-container">
            <NavLink to = "/projects-all" ><button className="blue-button">{text}</button></NavLink>
        </div>
    )
}

export default BlueButton;