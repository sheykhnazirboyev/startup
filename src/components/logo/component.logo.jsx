import React, {Component} from 'react';
import {NavLink, Link } from 'react-router-dom';
import './styles.logo.css';

import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';

import {SearchQuery} from '../../redux/project/project.actions';
import {bindActionCreators} from 'redux';



let strings = new LocalizedStrings({
	ru: Locale.ru,
	en: Locale.en,
	uz: Locale.uz,
})


class Logo extends Component
{
	constructor(props)
    {
        super(props);
        this.state = {
            hideLogoMenus: true,
            list: []
        }
        this.checkLogoMenus = this.checkLogoMenus.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


	checkLogoMenus()
	{
		this.setState({ hideLogoMenus: !this.state.hideLogoMenus })
	}

	handleChange(e)
	{
		this.props.SearchQuery(e.target.value);
		
	}
	
	render()
	{	
		let {hideLogoMenus} = this.state;


		let {lang} = this.props;

		strings.setLanguage(lang);

		return(
			<div className = "root-Logo">
				<div className = "container">
					<div className = "Logo">
						<div className = "brand">
						<Link to = "/">
							<img src="/logo.png"  alt="" className = "" />
						</Link>
							<button className = "logo-menu-btn" onClick = {this.checkLogoMenus}>
								<i  className = "fa fa-bars "></i>
							</button>
							
						</div>
						<div className = "menus">
							<NavLink exact = {true} to = "/projects-all"
								 className = "title-link" activeClassName = "activeClassName">
								 <span>{strings.projects}</span>
							</NavLink>
							<NavLink exact to = "/contacts" 
								className = "title-link"
								activeClassName = "activeClassName">
								<span>{strings.contacts}</span>
							</NavLink>
							<NavLink exact to = "/startupmarket-school" 
								className = "title-link"
								activeClassName = "activeClassName">
								<span className = "bounce">{strings.school_startupmarket}</span>
							</NavLink>
						</div>
						
						<div className = "search">
							<input type="text" name="search" id = "search" 
								onChange = {this.handleChange}  value = {this.props.searchQuery} />
							<label htmlFor="search"><i className = "fa fa-search"></i></label>

							
						</div>
						<div className = "create" >
							<NavLink to = "/create-project" className = "title-link" activeClassName = "activeClassName">
								<p>{strings.menu_create} <span className = "hide-500">{strings.menu_project}</span> +</p>
							</NavLink>
						</div>
						<div className = "button">
						<Link to = "/personal-cabinet">
							<button className = "large-sign">{this.props.usertoken ? "Profile" : strings.login }</button>
							<button className = "mobile-sign"><i className  ="fa fa-sign-in"></i></button>
						</Link>
						</div>
						<div className  = {hideLogoMenus ? "hide logo-mobile-menus" : "show logo-mobile-menus"}>
							<Link  to = "/projects-all" ><span>{strings.projects}</span></Link>
							<Link className = "title-link" to = "/contacts"><span>{strings.contacts}</span></Link>
							<Link className = "title-link" to = "/startupmarket-school" ><span>{strings.school_startupmarket}</span></Link>
						</div>
					</div>
				</div>
			</div>);
	}
	
}

const mapStateToProps = state => ({
	lang: state.lang.lang,
	usertoken: state.auth.usertoken,
	searchQuery: state.projects.searchQuery
})

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({SearchQuery}, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(Logo)  ;