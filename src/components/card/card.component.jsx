import React, { Component } from 'react';
import "./card.styles.css";

import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})



class Card extends Component {

    render() {

        
            
        const {title, pitch,category, address, main_image, created_date, launch_date,user, id} = this.props.project;
        const createdDate = new Date(created_date);
        const launchDate = new Date(launch_date);
        
         let {lang} = this.props;
         
         strings.setLanguage(lang);

         let image = "noimage";

        if(typeof main_image === "string")
        {
            try
            {
                image = JSON.parse(main_image).preview
            } catch {
                image = "image-not-found.png"
            }
        } else {
            image = main_image.preview
        }

       
        return (
            <div className="card">
                <Link  to = {`/individual-project/${id}`} className="card-image" 
                style = {{ backgroundImage: `url(${image}` }}>
                </Link>
                
                <div className="card-content">
                    <div className="top">
                        <Link  to = {`/individual-category/${category.id}`} >{category.name}</Link>
                        <p>{strings.card_author}: {user.realm}</p>
                    </div>
                    <h5 className="title"><Link className = "title-link"  to = {`/individual-project/${id}`}>{title}</Link></h5>
                    <p className="about">{pitch}</p>
                    <div className="project-info">
                        <div className="info-text">
                            <span>{strings.card_created_date}</span>
                            <p>{createdDate.getDate()} {createdDate.toLocaleString('default', {month: 'long'})}</p>
                        </div>
                        <div className="info-text">
                            <span>{strings.card_launch_date}</span>
                            <p>{launchDate.getDate()} {launchDate.toLocaleString('default', {month: 'long'})}</p>
                        </div>
                        <div className="info-text">
                            <span>{strings.card_location}</span>
                            <p><i className = "fa fa-map-marker"></i> {address}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    lang: state.lang.lang
})

export default connect(mapStateToProps)(Card) ;
