import React from 'react';
import "./create-project-tips.styles.css";

import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';

let strings = new LocalizedStrings(Locale)


 function CreateProjectTips(props) {
    strings.setLanguage(props.lang);
    return (
        <div className="create-project-tips-root">
            <div className="create-project-tips-container">
                <div className="content">
                    <h3>{strings.create_project_header}</h3>
                    <p>{strings.create_project_subtitle}</p>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    lang: state.lang.lang
})

export default connect(mapStateToProps)(CreateProjectTips) ;
