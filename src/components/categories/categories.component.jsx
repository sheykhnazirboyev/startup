import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import "./categories.styles.css";

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';





let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})


 class Categories extends Component {
    constructor(props)
    {
        super(props)
        this.checkImage = this.checkImage.bind(this);
    }
    
    checkImage(image){
        let result = "";

         if(typeof image === "string")
        {
            try
            {
                result = JSON.parse(image).preview
            } catch {
                result = "image-not-found.png"
            }
        } else {
            result = image.preview
        }

        return result;
    }

    
    render() {
         let {lang} = this.props;
        
         strings.setLanguage(lang);
         
        const {categories} = this.props;
        const categoryItems = categories && categories.map(c => ( c && c.image && c.name &&
            <Link key = {c.id} to = {`individual-category/${c.id}`}>
                <div className="item"  style = {{ backgroundImage: `url("${this.checkImage(c.image)}")`}}>
                    <div/>
                    <h4 className="title">{c.name}</h4>
                </div>
            </Link>
            ));
        return (
            <div className="categories-root">
                <div className="categories-container"  >
                    <nav className="nav">
                        <h4>{strings.categories_component_categories}</h4>
                        
                    </nav>
                    <div className="category-items">
                        {categoryItems}
                    </div>
                </div>
            </div>
        )
    }
}


export default (Categories) ;
