import React, {Component} from "react";
import PopularProject from "./pop-project.component";
import Slider from 'react-slick';
import {Spinner} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import "./pop-projects.styles.css";
import "./pop-project.styles.css";

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})


function SampleNextArrow(props) {
      const { className, style, onClick } = props;
      return (
        <div
          className={className}
          style={{ ...style, display: "none" }}
          onClick={onClick}
        />
      );
    }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "none"}}
        onClick={onClick}
      /> 
    );
  }



class PopProjects extends Component{
    constructor(props)
    {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
    }

     next() {
     
        this.slider.slickNext();
      }
      previous() {
        this.slider.slickPrev();
      }


    render(){

        const settings = {
          dots: false,
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true,
         nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
        };
        let {projects, isLoaded, lang} = this.props; 

         strings.setLanguage(lang);
        
        const allProjects = projects && projects.map(p => 
          (p.category && p.main_image && p.user && <PopularProject key = {p.id} project = {p}/>))

        return(
            <div className="pop-projects-root">
                <div className="pop-projects-container">

                    <nav className="pop-projects-header">
                        <h6 className="title">{strings.pop_project_header}</h6>
                        <div className="more">
                            <NavLink  to = "/projects-all" activeClassName = "activeClassName">{strings.top_slider_show_all}</NavLink>
                             <div className = "slider-buttons" >
                              <button className="button next" onClick={this.previous}>
                                <i className = "fa fa-chevron-left"></i>
                              </button>
                              <button className="button prev" onClick={this.next}>
                                <i className = "fa fa-chevron-right"></i>
                              </button>
                            </div>
                        </div>
                    </nav>
                        <Slider className = "slider" ref={c => (this.slider = c)} {...settings} >
                            {allProjects}
                        </Slider>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
  lang: state.lang.lang
})

export default connect(mapStateToProps)(PopProjects) ;