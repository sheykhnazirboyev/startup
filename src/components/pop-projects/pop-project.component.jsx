import React, {Component} from 'react';
import Moment from 'react-moment';
import {Link} from 'react-router-dom';
import "./pop-project.styles.css";
import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})



class PopularProject extends Component {
    render(){
    const {title, pitch,category:{name}, categoryId, address, main_image, created_date, launch_date,user, id} = this.props.project;
    let {lang} = this.props;

    strings.setLanguage(lang);

    const createdDate = new Date(created_date);

    let image = "noimage";

        if(typeof main_image === "string")
        {
            try
            {
                image = JSON.parse(main_image).preview
            } catch {
                image = "image-not-found.png"
            }
        } else {
            image = main_image.preview
        }
    

    return (
            <div className="project-container">
                    <Link to = {`/individual-project/${id}`} className="image" style = {{ backgroundImage: `url(${image}` }}>
                    </Link>
                <div className="content">
                        <Link className = "category" to = {`/individual-category/${categoryId}`} >{name}</Link>
                    <h3 className="title"> 
                        <Link className = "title-link" to = {`/individual-project/${id}`}>{title}</Link>
                    </h3>
                    <p className="description">
                        {pitch}
                    </p>
                    <div className="additional-info">
                        <p>{strings.card_author}: {user.realm}</p>
                        <p><i className = "fa fa-map-marker"></i> {address}</p>
                    </div>
                   
                    <div className="status-info">
                        <p>Status 2: Стадия планирования</p>
                        <p>{strings.card_created_date}: {createdDate.getDate()} {createdDate.toLocaleString('default', {month: 'long'})} </p>
                        <p>{strings.card_launch_date}: <Moment duration= {created_date} date = {launch_date} unit = "days" /></p>
                    </div>
                </div>
            </div>
       
    )};
}


const mapStateToProps = state => ({
  lang: state.lang.lang
})


export default connect(mapStateToProps)(PopularProject);