import React, {Component} from 'react';
import Card from '../card/card.component';
import {Form} from 'react-bootstrap';
import './individualCategory.styles.css';

class Category extends Component {
	
	constructor(props)
	{
		super(props);
		this.state = { sort: "default" }
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e)
	{
		this.setState({ sort: e.target. })
	}
	
	render()
	{
		const {category, projects} = this.props;
		
		const all = projects && projects.map(p => <Card project = {p} key = {p.id} />)
		return(
			<div className = "individual-category-root">
				<div className = "container">
				<div className = "individual-category-title">
					<div>
						<h2>{category}</h2>
					</div>
					 <div>
					 	<h2>Sort by</h2>
					 	<select name="sort" id="" value = {this.state.sort} onChange = {this.handleChange} >
					 		<option value="default">Default</option>
					 		<option value="invest">Invest</option>
					 	</select>
                        {/*<Form.Group controlId="exampleForm.ControlSelect1">
                            <h6>Sort by</h6>
                            <Form.Control 
                            as="select" onChange = {this.handleChange} >
                                <option default value = "ddefault">Default</option>
                                <option key = "invest" value="invest">Invest</option>
                            </Form.Control>
                        </Form.Group>*/}
                    </div>
				</div>
				<div className = "individual-category-cards">
						{all}
					</div>
				</div>
			</div>
			);
	}
}

export default Category;