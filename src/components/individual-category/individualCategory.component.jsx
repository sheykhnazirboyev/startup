import React, {Component} from 'react';
import Card from '../card/card.component';

import './individualCategory.styles.css';
import Filter from '../filter/filter.component';
import FilterFunction from '../filter/Filter.function';

class Category extends Component {
	constructor(props)
	{
		super(props);
		this.state = {"filter": "default" }
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e)
	{
		this.setState({ filter: e.target.value })
	}

	render()
	{

		const {category, projects} = this.props;
		
		let filtered = projects && FilterFunction(projects, this.state.filter);

		const all = projects && filtered.map(p => <span key = {p.id}>
		{
			p.category && p.main_image && p.user ? 
			<Card project = {p}  />
			: <h2>Noyt Found</h2>
		}
		</span>)
		return(
			<div className = "individual-category-root">
				<div className = "container">
				<div className = "individual-category-title">
					<h2>{category}</h2>
					<Filter handleChange  = {this.handleChange} filter = {this.state.filter} />
				</div>
				<div className = "individual-category-cards">
						{all}
					</div>
				</div>
			</div>
			);
	}
}

export default Category;