import React, { Component } from 'react';
import "./MiddleImage.styles.css";

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';
import Banner from '../../StaticPages/Main.Banner.json';
import {Link } from 'react-router-dom';
import {Modal, Button, Form} from 'react-bootstrap';
import {Field, reduxForm} from 'redux-form';

import axios from 'axios';





let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

 class MiddleImage extends Component {
    
    constructor(props)
    {
        super(props);
        this.state = {
            show: false,
            success: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.getImage = this.getImage.bind(this);
        this.renderInput = this.renderInput.bind(this);
    }

    handleSubmit(data){
        const newData = {...data, projectId: Banner.id}
        const url = "http://startupmarket.herokuapp.com/api/contacts";
        
        axios.post(url, newData)
        .then(response => this.setState({success: true}))
        
        this.handleClose()


        
    }

    handleClose(){
        this.setState({show: false});
        
    }
    handleShow(){
        this.setState({show: true});
    }
    
    renderInput(field){
     const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
            
            return (
                <div className={className}>
                <Form.Group controlId={field.placeholder}>
                    <h6>{field.label}</h6>
                    <Form.Control 
                        type="text" 
                        placeholder= {field.placeholder}
                        {...field.input}
                    />
                    <div className="error">
                        {field.meta.touched ? field.meta.error:''}
                    </div>
                </Form.Group>
                </div>
            )
    }

    getImage(image){
        let result = "";

         if(typeof image === "string")
        {
            try
            {
                result = JSON.parse(image).preview
            } catch {
                result = "image-not-found.png"
            }
        } else if(typeof image === JSON){
            result = image.preview
        }

        return result;
    }

    render() {
        let {lang} = this.props;

        strings.setLanguage(lang);

        if(this.state.success)
        {
            alert("Your message has been received")
        }


        return (
            <div className="Middle-image-container" style = 
            {{ backgroundImage: `url("${this.getImage(Banner.main_image)}")` }} >
                <div className="Middle-image"></div>
                <div className="Middle-image-content">
                    <h1 className="title">{Banner.title}</h1>
                    <p className="subtitle">
                        {Banner.pitch}
                    </p>
                    <div className="actions-container">
                        <button className="actions-more" >
                            <Link to = {`/individual-project/${Banner.id}`} 
                            className = "title-link text-light">{strings.middle_image_actions_more}</Link>
                        </button>
                        <button className="actions-invest" onClick = {this.handleShow}>
                            <span>{strings.middle_image_actions_invest}</span>
                        <i className="fa fa-angle-right"></i>
                        <i className="fa fa-angle-right"></i>
                        </button>

                        <Modal show={this.state.show} onHide={this.handleClose}>
                            <form onSubmit = {this.props.handleSubmit(this.handleSubmit)}>
                            <Modal.Header closeButton>
                              <Modal.Title>{Banner.title}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div>
                                    <Field
                                        name = "name"
                                        label = "Type your name"
                                        placeholder = "name"
                                        component = {this.renderInput}
                                     />
                                </div>
                                <div>
                                    <Field
                                        name = "phone"
                                        label = "Your phone number"
                                        placeholder = "Phone number"
                                        component = {this.renderInput}
                                     />
                                </div>
                                <div>
                                    <Field
                                        name = "country"
                                        label = "Your country"
                                        placeholder = "Your country"
                                        component = {this.renderInput}
                                     />
                                </div>
                            
                            </Modal.Body>
                            <Modal.Footer>
                              <Button variant="secondary" onClick={this.handleClose}>
                                Close
                              </Button>
                              <Button type = "submit" variant = "primary">
                                Save
                              </Button>
                            </Modal.Footer>
                            </form>
                          </Modal>
                    </div>
                </div>
            </div>
        )
    }
}

function validate(values){
    const errors = {};

    if(!values.name)
    {
        errors.name = "The name is required"
    }

    if(!values.country)
    {
        errors.country = "Country is required"
    }

    if(!values.phone)
    {
        errors.phone = "Phone is required"
    }

    if(values.phone && (values.phone.length < 9))
    {
        errors.phone = "Invalid phone number"
    }



    return errors;
}

export default  reduxForm({
    form: "invest-form",
    validate
})(MiddleImage) ;
