import React from 'react';
import "./card-container.styles.css";

 function CardContainer(props) {
    return (
        <div className="card-container-root">
            <div className="card-container">
                {props.children}
            </div>
        </div>
    )
}

export default CardContainer;
