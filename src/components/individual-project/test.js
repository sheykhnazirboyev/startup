import React from 'react';
import "./individual-project.styles.css";
import room1 from "../../img/room1.jpg";
import Moment from 'react-moment';
import {Link} from 'react-router-dom';
import BlueButton from '../blue-button/blue-button.component';

 function IndividualProjectCard(props) {
    
    const project = props.project;
    const createdDate = new Date(project.created_date);
    const launcDate = new Date(project.launch_date);
    const bag = project.category.name;
    return (
            <div className="individual-project-card-root">
            <div className="individual-project-card-container">
                <div className="card-image-container">
                    <div className="main-image">
                        <img src={`/img/projects/${project.main_image}`} alt={project.title}/>
                    </div>
                    <div className="secondary-image-container" >
                        <img className="secondary-image" src={room1} />
                        <img className="secondary-image" src={room1} />
                        <img className="secondary-image" src={room1} />
                    </div>
                </div>

                <div className="card-content-container">
                <Link to = {`/individual-category/${project.categoryId}`}><span>{project.category.name}</span></Link>
                    <h3>{project.title}</h3>
                    <p>{project.details}</p>
                    <div className="additional-info">
                        <p>Создатель: Andrew Noah</p>
                        <p> <i className = "fa fa-map-marker"></i>{project.address}</p>
                    </div>
                    
                    <div className="status-info">
                        <p><span>Status 2:</span> Стадия планирования</p>
                        <p>Запущен: 
                            {createdDate.getDate()} {createdDate.toLocaleString('default', {month: 'long'})}
                        </p>
                        <p>Осталось: <Moment duration= {project.created_date} date = {project.launch_date} unit = "days" /></p>
                    </div>
                    <button className="invest-button">Инвестировать</button>
                    <p>Поделиться:</p>
                    <div className="share-in-social">
                        <Link to = "" className = "title-link" ><i className = "fa fa-facebook-square"></i></Link>
                        <Link to = "" className = "title-link"><i className = "fa fa-instagram"></i></Link>
                        <Link to = "" className = "title-link"><i className = "fa fa-youtube"></i></Link>
                        <Link to = "" className = "title-link"><i className = "fa fa-twitter"></i></Link>
                        <Link to = "" className = "title-link"><i className = "fa fa-google"></i></Link>
                    </div>
                </div>
            </div>
            </div>
    )
}

export default IndividualProjectCard;
