import React, {useState} from 'react';
import "./individual-project.styles.css";
import Moment from 'react-moment';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {Modal, Button, Form} from 'react-bootstrap';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';
import {Field, reduxForm} from 'redux-form';
import axios from 'axios';



let strings = new LocalizedStrings(Locale)



 function IndividualProjectCard(props) {
    
    const [show, setShow] = useState(false)
    const [success, setSuccess] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleResponse = () => setSuccess(true)
    


    const renderInput = (field) => {
     const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
            
            return (
                <div className={className}>
                <Form.Group controlId={field.placeholder}>
                    <h6>{field.label}</h6>
                    <Form.Control 
                        type="text" 
                        placeholder= {field.placeholder}
                        {...field.input}
                    />
                    <div className="error">
                        {field.meta.touched ? field.meta.error:''}
                    </div>
                </Form.Group>
                </div>
            )
    }

    const handleSubmit = (data) => {
        const newData = {...data, projectId: props.project.id}
        const url = "http://startupmarket.herokuapp.com/api/contacts";
        
        axios.post(url, newData)
        .then(response => {handleResponse()   })
        
        handleClose()
    }

    const chechImage = (image) => {
        let result = "";

         if(typeof image === "string")
        {
            try
            {
                result = JSON.parse(image).preview
            } catch {
                result = "image-not-found.png"
            }
        } else {
            result = image.preview
        }

        return result;
    }

    const project = props.project;

    const createdDate = new Date(project.created_date);

    strings.setLanguage(props.lang);

    if(success)
    {
        alert("Your message has been received");
    }
    
    return (
            <div className="individual-project-card-root">
            <div className="individual-project-card-container">
                <div className="card-image-container">
                    <div className="main-image">
                        <img src={`${chechImage(project.main_image)}`} alt={project.title}/>
                    </div>
                </div>

                <div className="card-content-container">
                <Link to = {`/individual-category/${project.categoryId}`}><span>{project.category.name}</span></Link>
                    <h3>{project.title}</h3>
                    {project.pitch}
                    <div className="additional-info">
                        <p>{strings.card_author}: {project.user && project.user.realm}</p>
                        <p> <i className = "fa fa-map-marker"></i>{project.address}</p>
                    </div>
                    
                    <div className="status-info">
                        <p><span>{strings.card_station} 2:</span> Стадия планирования</p>
                        <p>{strings.card_created_date}: 
                            {createdDate.getDate()} {createdDate.toLocaleString('default', {month: 'long'})}
                        </p>
                        <p>{strings.card_launch_date}: <Moment duration= {project.created_date} date = {project.launch_date} unit = "days" /></p>
                    </div>
                    <div className = "full-width">
                    {success  ? "" : 
                        <button  className = "invest-button" onClick={handleShow}>
                            {strings.middle_image_actions_invest}
                        </button>
                    }

                        <Modal show={show} onHide={handleClose}>
                            <form onSubmit = {props.handleSubmit(handleSubmit)}>
                            <Modal.Header closeButton>
                              <Modal.Title>Modal heading</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div>
                                    <Field
                                        name = "name"
                                        label = "Type your name"
                                        placeholder = "name"
                                        component = {renderInput}
                                     />
                                </div>
                                <div>
                                    <Field
                                        name = "phone"
                                        label = "Your phone number"
                                        placeholder = "Phone number"
                                        component = {renderInput}
                                     />
                                </div>
                                <div>
                                    <Field
                                        name = "country"
                                        label = "Your country"
                                        placeholder = "Your country"
                                        component = {renderInput}
                                     />
                                </div>
                            
                            </Modal.Body>
                            <Modal.Footer>
                              <Button variant="secondary" onClick={handleClose}>
                                Close
                              </Button>
                              <Button type = "submit" variant = "primary">
                                Save
                              </Button>
                            </Modal.Footer>
                            </form>
                          </Modal>
                    </div>
                    
                    {/*<button className="invest-button">{strings.middle_image_actions_invest}</button>*/}
                    <p>{strings.card_share }:</p>
                    <div className="share-in-social">
                    {project.facebook_link && 
                        <a href = {project.facebook_link} className = "title-link" ><i className = "fa fa-facebook-square"></i></a> }

                    {project.youtube_link &&
                        <a href = {project.youtube_link} className = "title-link"><i className = "fa fa-youtube"></i></a>}

                    {project.linkedin_link &&
                        <a href = {project.linkedin_link} className = "title-link"><i className = "fa fa-linkedin-square"></i></a>}

                    {project.telegram_link &&
                        <a href = {project.telegram_link} className = "title-link"><i className = "fa fa-telegram"></i></a>}
                    </div>
                </div>
            </div>
            </div>
    )
}

function validate(values){
    const errors = {};

    if(!values.name)
    {
        errors.name = "The name is required"
    }

    if(!values.country)
    {
        errors.country = "Country is required"
    }

    if(!values.phone)
    {
        errors.phone = "Phone is required"
    }

    if(values.phone && (values.phone.length < 9))
    {
        errors.phone = "Invalid phone number"
    }



    return errors;
}

const mapStateToProps = (state) => ({
    lang: state.lang.lang,
});

export default reduxForm({
    validate,
    form: "invest-form"})(connect(mapStateToProps)(IndividualProjectCard)) ;


