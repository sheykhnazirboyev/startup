import React from 'react';
import TabPanel from '../tab-panel/tab-panel.component';
import "./personal-projects-tab.styles.css"

 function PersonalProjectsTab() {
    return (
       
        <div className="personal-projects-tab-root">
            <div className="personal-projects-tab-container">
                
                <div className="tab-item">
                    <div className="tab-text">
                        <h6>Созданные проекты</h6>
                        <p>Список созданных проектов</p>
                    </div>
                    <div className="tab-icon">
                        <i className="fa fa-lightbulb-o"></i>
                    </div>
                </div>
                <div className="tab-item">
                    <div className="tab-text">
                        <h6>Инвесируемые проекты</h6>
                        <p>Проекты которые вы поддержали</p>
                    </div>
                    <div className="tab-icon">
                    <i className="fa fa-money" aria-hidden="true"></i>
                    </div>
                </div>
                <div className="tab-item">
                    <div className="tab-text">
                        <h6>Покупка</h6>
                        <p>Список покупок</p>
                    </div>
                    <div className="tab-icon">
                    <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PersonalProjectsTab;