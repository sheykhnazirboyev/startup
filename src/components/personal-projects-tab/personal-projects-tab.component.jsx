import React, {Component} from 'react';
import TabPanel from '../tab-panel/tab-panel.component';
import TopSlider from '../topSlider/topSlider.component';
import "./personal-projects-tab.styles.css"

 class  PersonalProjectsTab extends Component {

    
    render(){
        let res = null;

        const {projects} = this.props;
       
       if(this.props.projectsCount > 0){
            res = projects && (projects.length > 0) ?  <div style = {{ width: "100%"}}>
                    <TopSlider projects = {projects} header = {false} 
                                 showAll = {false} />
                </div>
                :<h2 className = "text-muted text-center">Not found</h2>
        }   else    {
            res = <div className = "no-projects">
                        <div className = "no-projects-text">
                            <h2 className = "text-center">No Projects</h2>
                        </div>
                   </div>
        }

    return (
            <TabPanel styled = "personal">
            
            <div  label="Description"  title = "Созданные проекты"  
                   subtitle = "Список созданных проектов" 
                   icon = "fa fa-lightbulb-o" className = "personal-tabs">
                {res}
            </div>
            <div  label="Invest"  title = "Инвесируемые проекты" 
                  subtitle = "Проекты которые вы поддержали" icon = "fa fa-money">
                <div className = "no-projects-text">
                        <h2 className = "text-center">No Projects1</h2>
                    </div>
            </div>
            <div  label="Sale"  title = "Покупка" subtitle = "Список покупок" 
                                icon = "fa fa-shopping-cart">
                <div className = "no-projects-text">
                        <h2 className = "text-center">No Projects</h2>
                    </div>
            </div>
            </TabPanel>
            
    )
}
}

export default PersonalProjectsTab;