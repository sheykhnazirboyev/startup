import React, {useState} from 'react';
import {Modal, Button, Form} from 'react-bootstrap';
import {reduxForm, Field} from 'redux-form';
import axios from 'axios';

function ModalComponent(props) {

    const [show, setShow] = useState(false)
    

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    
   const renderTextareaField = (field) => {
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <div className={className}>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                        <h6>{field.label}</h6>
                        <Form.Control 
                        as="textarea" 
                        rows="4" 
                        placeholder={field.placeholder}
                        {...field.input} />
                </Form.Group>
                 <div className="error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </div>
        )
    }

    const renderInput = (field) => {
    const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
            
            return (
                <div className={className}>
                <Form.Group controlId={field.placeholder}>
                    <h6>{field.label}</h6>
                    <Form.Control 
                        type="text" 
                        placeholder= {field.placeholder}
                        {...field.input}
                    />
                    <div className="error">
                        {field.meta.touched ? field.meta.error:''}
                    </div>
                </Form.Group>
                </div>
            )
    }

    const handleSubmit = (data) => {

        const url = "http://startupmarket.uz/api/feedbacks";
        
        axios.post(url, data)
        .then(response => {alert("Your message has been received"); this.props.reset()} )
        .catch(error => console.log(error))
        
        handleClose()
    }


    
    
  return (
    <div>
      <span variant="default"  className = "modal-button" onClick={handleShow}>
        {props.name}
      </span>
      <Modal show={show} onHide={handleClose}>
            <form onSubmit = {props.handleSubmit(handleSubmit)}>
            <Modal.Header closeButton>
              <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    <Field
                        name = "name"
                        label = "Type your name"
                        placeholder = "name"
                        component = {renderInput}
                     />
                </div>
                <div>
                    <Field
                        name = "phone"
                        label = "Your phone number"
                        placeholder = "Phone number"
                        component = {renderInput}
                     />
                </div>
                <div>
                    <Field
                        name = "email"
                        label = "Your email"
                        placeholder = "Your email"
                        component = {renderInput}
                     />
                </div>
                 <div>
                    <Field
                        name = "message"
                        label = "Your message"
                        placeholder = "Your message"
                        component = {renderTextareaField}
                     />
                </div>
            
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button type = "submit" variant = "primary">
                Save
              </Button>
            </Modal.Footer>
            </form>
          </Modal>
    </div>
  );
}

function validate(values){
    const errors = {};

    if(!values.name)
    {
        errors.name = "The name is required"
    }

    if(!values.email )
    {
      errors.email = "Email required"
    } else if(values.email.length < 5 || values.email.indexOf("@") < 0) {
      errors.email = "Invalid email"
    }

    if(!values.phone)
    {
        errors.phone = "Phone is required"
    }

    if(values.phone && (values.phone.length < 9))
    {
        errors.phone = "Invalid phone number"
    }
    if(!values.message)
    {
      errors.message = "Message is required"
    }



    return errors;
}

export default  reduxForm({
  validate,
  form: "invest-form"
})(ModalComponent);