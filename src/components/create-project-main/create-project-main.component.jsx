import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Form from 'react-bootstrap/Form';

import {connect} from "react-redux";
import {addProject} from "../../redux/project/project.actions";
import { Field, reduxForm } from 'redux-form';
import TabPanel from "../../components/tab-panel/tab-panel.component";
import "./create-project-main.styles.css";
import Card from './create-card';

import { EditorState, convertToRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import { unemojify } from "node-emoji";

import FormData from 'form-data'
import axios from 'axios';
import {Redirect} from 'react-router-dom';
import {Spinner} from 'react-bootstrap';

import ModernDatepicker from 'react-modern-datepicker';


import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';
import ModalComponent from '../Modal/Modal.component';


let strings = new LocalizedStrings(Locale);


 class CreateProjectMain extends Component {
    constructor(props){
        super(props);
        this.state = {
            editorState: EditorState.createEmpty(),
            main_image: '',
            loading_img: false,
            success:false,
            project_id: "",
            "error_upload":""
        }
        this.handleChange = this.handleChange.bind(this);
        this.renderInputField = this.renderInputField.bind(this);
        this.renderTextareaField = this.renderTextareaField.bind(this);
        this.adaptFileEventToValue = this.adaptFileEventToValue.bind(this);
        this.FileInput = this.FileInput.bind(this);
        this.EditorRender = this.EditorRender.bind(this);
        this.ImageUpload = this.ImageUpload.bind(this);
        this.handleActiveTab = this.handleActiveTab.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.clearImage = this.clearImage.bind(this);
        this.rederDate = this.renderDate.bind(this);

    }

    componentDidMount()
    {
        window.scroll(0, 0)
    }
   
    clearImage()
    {
        const {reset} = this.props;
        reset();
        this.setState({ main_image: "" })
    }

    handleSubmit(props)
    {
        let image = "";
        try
        {
            image = JSON.stringify(this.state.main_image)
            console.log("JSON")
        } catch {
            image = this.state.main_image
        }
        const data =  {...this.props.form.PostProject.values,
                        created_date: new Date(),
                        main_image: image,
                        userId: this.props.usertoken.userId
                      };

        const config = {
            method: 'post',
            headers: {
                     'Content-Type': 'application/json',
                    }
            }
        const url = "http://startupmarket.uz/api/projects"
        axios.post(url,data, {config} )
        .then(response => {this.setState({ success: true, project_id: response.data.id } )})
        .catch(error => console.log(error));

    }

    handleActiveTab(){
        this.setState({active:!this.state.active})

        window.scrollTo(300, 300)
    }

    ImageUpload(e)
    {
        this.setState({ loading_img: true })
        const URL = "http://startupmarket.uz/api/projects/upload";
        const file = new Blob([e.target.files[0]], { type: 'image/png/jpeg' });
        
        const formData = new FormData();
        formData.append('file', file, file.filename);

         const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        axios.post(URL, formData, config)
        .then(res => 

            this.setState({main_image: res.data, loading_img: false}))
        .catch(error => this.setState({error_upload: "failed", loading_img: false}));
    }
    

    handleChange(e){
        const {name, value} = e.target;
        console.log(this.state);
        this.setState({[name]: value});
    }

    EditorRender(field)
    {
        const onEditorStateChange = editorState => {
                const { onChange, value } = field.input;

                const newValue = unemojify(
                  draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
                );

                if (value !== newValue) {
                  onChange(newValue);
                }

                this.setState({
                  editorState
                });
        };
    
        field.input.onChange(
          draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
        );
    
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <div className = {className} >
                <Editor
                  controlId="EditorExample"
                  toolbarClassName="editor-toolbar"
                  editorClassName="editor"
                  editorState={this.state.editorState}
                  onEditorStateChange={onEditorStateChange}
                  {...field.input}
                  toolbar={{
                    inline: { inDropdown: true },
                    list: { inDropdown: true },
                    textAlign: { inDropdown: true },
                    link: { inDropdown: true },
                    history: { inDropdown: true }
                  }}
                />
                <div className="error editor-error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </div>
        );
    }

    renderInputField(field){                
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        
        return (
            <div className={className}>
            <Form.Group controlId={field.placeholder}>
                <h6>{field.myLabel}</h6>
                <Form.Control 
                    type="text" 
                    placeholder= {field.placeholder}
                    {...field.input} 
                />
                <div className="error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </Form.Group>
            </div>
        )
    }



     renderTextareaField(field){
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <div className={className}>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                        <h6>{field.myLabel}</h6>
                        <Form.Control 
                        as="textarea" 
                        rows="4" 
                        placeholder={field.placeholder}
                        {...field.input} />
                </Form.Group>
                 <div className="error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </div>
        )
    }

    renderSelectField(field){
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <div className={className}>
                <Form.Group controlId="exampleForm.ControlSelect1">
                        <h6>{field.myLabel}</h6>
                        <Form.Control {...field.input} 
                        as="select">
                        <option default value = "-1">{field.default}</option>
                        {field.categories && field.categories.map(c => 
                            <option key = {c.id} value={c.id}>{c.name}</option>
                        )}
                        </Form.Control>
                    </Form.Group>
                 <div className="error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </div>
        )
    }

    adaptFileEventToValue = delegate => e => {
         /*  this.setState({
            main_image: URL.createObjectURL(e.target.files[0])
       });*/
           this.ImageUpload(e)

        return delegate(e.target.files[0]);
    };
    

    FileInput({ 
          input: { value: omitValue, onChange, onBlur, ...inputProps }, 
          meta,
          meta: omitMeta, 
          ...props 
        })
        {
        const className = `form-input ${meta.touched && meta.error ? 'has-error':''}`;
          return (
            <div className = {className} >
                <label className="upload-image-button" htmlFor="photo">{strings.create_project_ad_image_btn}</label>
                <input
                  style = {{ display: "none" }}
                  id = "photo"
                  onChange={this.adaptFileEventToValue(onChange)}
                  onBlur={this.adaptFileEventToValue(onChange)}
                  type="file"
                  {...props.input}
                  {...props}
                />
                <div className="error">
                    {meta.touched ? meta.error:''}
                </div>
            </div>
          );
        };

    renderDate(field)
    {
        
        const className = `form-input ${field.meta.touched && field.meta.error ? "has-error" : ""}`
        return(
            <div className = {className}>
                <h6>{field.mylabel}</h6>
                <ModernDatepicker 
                  date={ field.input.value} 
                  format={'YYYY-MM-DD'} 
                  showBorder        
                  onChange={(date) => field.input.onChange(date)}
                  placeholder={field.placeholder}
                  {...field.input}
                />
                <div className = "error">
                    {field.meta.touched ? field.meta.error : ""}
                </div>
            </div>
            )
    }

    render(){
        
        

        const { main_image, project_id} = this.state;
        
        strings.setLanguage(this.props.lang);

        const allCategories = this.props.categories && this.props.categories;
        const {handleSubmit, pristine, submitting} = this.props;

        let imgContainer = "";
        if(this.state.loading_img)
        {
            imgContainer = <div className="upload-image">
                    <div className = "spinner-block"  >
                    <Spinner className = "spinner" animation="grow"  />
                    </div>
                </div>
        } else {
            imgContainer =  main_image ? <img src={this.state.main_image.preview} alt={this.state.main_image} /> 
                :(<div className="upload-image">
                    {   this.state.error_upload ? <i className="fa fa-exclamation-triangle " aria-hidden="true"></i> 
                        : (<>
                            <i className="fa fa-upload" aria-hidden="true"></i>
                            <h5>{strings.create_project_upload_image}</h5>
                            </>
                           )
                    }
                    
                    
                </div>)
        }

        if(this.state.success)
        {
            return <Redirect to = {`/individual-project/${project_id}`} />
        }
        
        return (
            <div>
            <form onSubmit={handleSubmit(this.handleSubmit) } encType="multipart/form-data" >
            {this.props.usertoken ? 
            <TabPanel onActiveTab={this.state.active} activeLabel= "Детали" >
            <div label="Общая информация" title = {strings.create_general_info}>
                 <div className="create-project-main-root">
                <div className="create-project-main-container">
                
                
                <div className="main-container">
                
                <div className="custom-form-group">
                    <Field
                        myLabel={strings.create_project_name_of_project}
                        placeholder = {strings.create_project_name_of_project}
                        name="title"
                        component={this.renderInputField}
                    />
                    <span>{strings.create_project_name_of_project_subtitle1}</span>
                    <span>{strings.create_project_name_of_project_subtitle2}</span>
                </div>

                <div className="custom-form-group">
                <Field
                        placeholder={strings.create_project_description_of_project}
                        myLabel={strings.create_project_description_of_project}
                        name="pitch"
                        component={this.renderTextareaField}
                        value={this.props.value}
                    />
                    <span>{strings.create_project_description_subtitle}</span>
                </div>

                <div className="custom-form-group">
                <Field  
                        default={strings.create_project_category_of_project}
                        myLabel={strings.create_project_get_category_of_project}
                        name="categoryId"
                        categories = {allCategories}
                        component={this.renderSelectField}
                    />
                </div>
                <div className="custom-form-group">
                    <Field  
                        myLabel={strings.create_project_location_of_project}
                        name="address"
                        placeholder = {strings.create_project_location_of_project}
                        component={this.renderInputField}
                    />
                </div>
                <div className="custom-form-group">
                    
                    <Field  
                        myLabel="Facebook link"
                        name="facebook_link"
                        placeholder = "Facebook link"
                        component={this.renderInputField}
                    />
                    <Field  
                        myLabel="Linkedin link"
                        name="linkedin_link"
                        placeholder = "Linkedin link"
                        component={this.renderInputField}
                    />
                    <Field  
                        myLabel="Telegram link"
                        name="telegram_link"
                        placeholder = "Telegram link"
                        component={this.renderInputField}
                    />
                </div>
                <div className = "custom-form-group">
                    <Field
                        name = "launch_date"
                        mylabel = "Select launch date"
                        placeholder = "Select launch date"
                        component = {this.renderDate}
                     />
                </div>
                <div className="custom-form-group">
                <h6>{strings.create_project_cover_of_project}</h6>
                    <div className="upload-image-container">
                            { imgContainer}
                        <div className="uploading-actions">
                            <div className="image-actions">
                                <Field name="image"  component={this.FileInput}  type="file"/>
                                <span className = "text-muted">{strings.create_project_main_image_of_project}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className = "custom-form-group">
                <div className = "upload-video-container">
                    
                    <div className = "uploading-actions">
                    
                          <div className="video-actions">

                            <Field 
                                myLabel = {strings.create_project_video_of_project}
                                name = "youtube_link"
                                placeholder = "Only Youtube link"
                                component = {this.renderInputField}
                            /> 
                            </div>
                    </div>
                </div>
                </div>
               
                <div className="clear-or-next-container">
                    <button type = "button" onClick = {this.clearImage} className = "clear-draft">
                        {strings.create_project_clear_info}
                    </button>

                    <button type = "button" 
                        onClick={this.handleActiveTab}
                       className="next-button" >{strings.create_project_next_btn}</button>
                </div>
            </div>
                <div className="secondary-container">
                    <Card image = {main_image.preview} />
                    <div className="preview-and-questions " >
                    <ModalComponent name = {strings.card_give_question} />
                    <button className="next-button full-width" type = "submit">{strings.create_project_moderation_btn}</button>
                    </div>
                </div>
            </div>
            </div>
            </div>
            <div label="Детали" title = {strings.create_details} >
            
                <div className = "text-editor">

                <div className = "editor-wrapper">
                    <div className = "editor-wrapper-header">
                        <h3>{strings.create_project_description}</h3>
                    </div>
                    
                    <Field 
                        name = "details"
                        component = {this.EditorRender}
                    />
                    
                    <div className = "editor-wrapper-text">
                        <p className = "text-muted editor-info">
                        {strings.create_project_editor_subtitle}
                         </p>
                    </div>
                </div>
                    <div className="description-side-content"  >
                        <div className="help-desk" >
                            <h4>{strings.card_help}</h4>
                            <p><Link className = "title-link" to = "/startupmarket-school">{strings.card_become_investor}?</Link></p>
                            <p><Link  className = "title-link" to = "/startupmarket-school">{strings.card_how_create}?</Link></p>
                            <Link  to="/startupmarket-school">{strings.school_startupmarket}</Link>
                        </div>
                        <ModalComponent name = {strings.card_give_question} />
                        <button  className="next-button full-width"  
                        type = "submit"
                        disabled={submitting || pristine} >{strings.create_project_moderation_btn}</button>
                    </div>
                    
                </div>

                
            </div>
            <div label="Способы оплаты" title = {strings.create_payment}>
                <h2>{strings.create_payment}</h2>
            </div>

            </TabPanel>
            : 
            <div className = "create-failed-div">
                <h4>First Create Account</h4>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page 
                . Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 
                Many desktop publishing</p>
                <Link className = "create-register-btn" to = "/register">Register <i className = "fa fa-plus-circle"></i></Link>
                <Link className = "create-login-btn" to = "/login">Login  <i className = "fa fa-sign-in"></i></Link>
            </div>
            }
             </form>
           </div>  
           
        )
    }
}
function validate(values){
    
    const errors = {};


    
    if(!values.title){
        errors.title = "The title is empty";
    
    }

    if(!values.pitch){
        errors.pitch = "The description is empty"
    }


    if(!values.address)
    {
        errors.address = "The location is empty" 
    }

    if(values.categoryId < 0 || !values.categoryId)
    {
        errors.categoryId = "Select category"
    }
    
    if(!values.launch_date)
    {
        errors.launch_date = "Invalid date"
    } 
    
    if(!values.image)
    {
        errors.image = "The image is empty"
    }

    if(!values.youtube_link)
    {
        errors.youtube_link = "The video link is empty"
    }

    if(!values.details)
    {
        errors.details = "The details section is empty"
    }
        
    if(values.details )
    {
        if(values.details.length <= 8)
        {
            errors.details = "The details section is empty"
        }
    }

    return errors;
}

const mapStateToProps = (state) => ({
    form: state.form,
    lang: state.lang.lang,
    usertoken: state.auth.usertoken
})

const mapDispatchToProps = (dispatch) => ({
    addProject: (projectDetails) => dispatch(addProject({projectDetails}))
});
export default reduxForm({
    validate,
    form:'PostProject'
})(connect(mapStateToProps,mapDispatchToProps)(CreateProjectMain))
