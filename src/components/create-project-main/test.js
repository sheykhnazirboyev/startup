/**
 * GitHub: https://github.com/remarkablemark/html-react-parser
 * Medium: https://medium.com/@remarkablemark/an-alternative-to-dangerously-set-innerhtml-64a12a7c1f96
 * Blog:   https://remarkablemark.org/blog/2016/10/07/dangerously-set-innerhtml-alternative/
 */

class App extends React.Component {
  htmlToReact() {
    const html = `
      <h1 style="font-family: Arial;">
        html-react-parser
      </h1>
    `;
    return HTMLReactParser(html);
  }

  htmlToReactWithReplace() {
    const html = `
      <p>
        This is a demo of <span id="replace">placeholder</span>
      </p>
    `;
    const replace = domNode => {
      if (domNode.attribs && domNode.attribs.id === 'replace') {
        return (
          <code>
            <a
              href="https://github.com/remarkablemark/html-react-parser"
              target="_blank"
            >
              html-react-parser
            </a>
          </code>
        );
      }
    };
    return HTMLReactParser(html, { replace });
  }

  render() {
    return (
      <React.Fragment>
        {this.htmlToReact()}
        {this.htmlToReactWithReplace()}
      </React.Fragment>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
