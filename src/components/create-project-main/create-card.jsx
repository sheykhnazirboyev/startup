import React, {Component} from 'react';
import {Link } from 'react-router-dom';
import {connect} from 'react-redux';

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';


let strings = new LocalizedStrings(Locale);

class CreateCard extends Component
{   
    static defaultProps = {
        
            title:"Titlte...",
            pitch:"Details...",
            category:"category...",
            location:"Location...",
            author: "Author"
        
           
    }

    render()
    {   const {title, pitch,category, 
                address,image, usertoken:{user} } = this.props;

        strings.setLanguage(this.props.lang);
        
        const {PostProject} = this.props.form;
        const informations = PostProject && PostProject.values;
        
        const created_date = new Date();
        const launch_date = informations 
                            && informations.launch_date && (new Date(informations.launch_date));
        
        return(
            <div className="card">
                <div  className="card-image" style = 
                {{ backgroundImage: `${image}` }}>
                <img style = {{ width: "100%" }} src={image} alt=""/>
                </div>
                
                <div className="card-content">
                    <div className="top">
                        <Link to = "#" >
                            { (informations && informations.categoryId) ?  informations.categoryId : category}
                        </Link>
                        <p>{strings.card_author}: {user.realm}</p>
                    </div>
                    <h5 className="title"><Link className = "title-link" to = "#" >
                        { (informations && informations.title) ?  informations.title : title}
                    </Link></h5>
                    <p className="about">
                        { (informations && informations.pitch) ?  informations.pitch : pitch}
                    </p>
                    <div className="project-info">
                        <div className="info-text">
                            <span>{strings.card_created_date}</span>
                            <p>
                                {created_date.getDate()} 
                                {created_date.toLocaleString('default', {month: 'long'})}
                            </p>
                        </div>
                        <div className="info-text">
                            <span>{strings.card_launch_date}</span>
                            <p>
                                {launch_date && launch_date.getDate()} 
                                {launch_date && launch_date.toLocaleString('default', {month: 'long'})}
                            </p>
                        </div>
                        <div className="info-text">
                            <span>{strings.card_location}</span>
                            <p><i className = "fa fa-map-marker"></i> 
                            { (informations && informations.address) ?  informations.address : address}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            );
    }
}


const mapStateToProps = state => ({
    form: state.form,
    lang: state.lang.lang,
    usertoken: state.auth.usertoken
})

export default connect(mapStateToProps)(CreateCard);