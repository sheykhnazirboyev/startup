import React, { Component } from 'react';
import "./Lastbar.styles.css";
import {NavLink} from 'react-router-dom';
import menus from '../../StaticPages/Menus.json'
import {connect} from 'react-redux';
import Locale from '../../Locale/locale.json';
import LocalizedStrings from 'react-localization';


let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

class Lastbar extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            hideInMobile: true
        }
        this.checkAllMenus = this.checkAllMenus.bind(this);
    }

    checkAllMenus()
    {
        this.setState({ hideInMobile: !this.state.hideInMobile })
    }

    componentDidUpdate(newProps)
    {
        if(newProps.changes !== this.props.changes)
        {
            this.setState({hideInMobile: true })
        }
    }

    render() {
        let  {hideInMobile} = this.state;
        
        strings.setLanguage(this.props.lang);
        
        const all = menus.menus;
        
        const Menus = all.map(menu =>
            <li key = {menu.id}>
                <NavLink activeClassName = "activeClassName" className = "title-link" exact to = {`/individual-category/${menu.id}`}>
                    {menu[`${this.props.lang}`]}
                </NavLink>
            </li> 
        )
        
        return (
            <div className = "Lastbar-container">
                <div className = "mobile-menu">
                    <div>Menu</div>
                    <div>
                        <button className = "mobile-menu-btn" onClick = {this.checkAllMenus}><i  className = "fa fa-bars "></i></button> 
                    </div>
                </div>
                <ul className = {hideInMobile ? "hide Lastbar-menus" : "show Lastbar-menus"}>
                   {Menus}
                </ul>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    lang: state.lang.lang,
    changes: state.projects,
})

export default connect(mapStateToProps)(Lastbar) ;
