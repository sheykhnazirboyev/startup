import React from "react";
import "./social-media.styles.css";
import Socials from  "../../Locale/Layout.json"

const SocialMedia = () =>(
    <div className="social-media-container">
        <a href={Socials.socials.facebook} ><i className="fa fa-facebook"></i></a>
        <a href={Socials.socials.twitter}><i  className="fa fa-twitter"></i></a>
        <a href={Socials.socials.google} ><i className="fa fa-google"></i></a>
        <a href={Socials.socials.instagram} ><i className="fa fa-instagram"></i></a>
    </div>
);

export default SocialMedia;