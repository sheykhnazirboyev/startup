import React, {Component} from "react";
import "./footer.styles.css";
import SocialMedia from "./social-media.component";
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {GetPages, GetPagesFailure} from '../../redux/page/page.actions';
import {GetCategories, GetCategoriesFailure} from '../../redux/category/category.actions';
import axios from 'axios';
import {reduxForm, Field} from 'redux-form'
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';

import {ChangeLang} from '../../redux/lang/lang.actions';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

class  Footer extends Component{
    
    constructor(props)
    {
        super(props);
        
        this.renderInputField = this.renderInputField.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.langChange = this.langChange.bind(this);
    }

    langChange(e)
    {
        this.props.ChangeLang(e.target.value)
    }

    componentDidMount()
    {
        const url_pages = `http://startupmarket.uz/api/pages`;

        const filter = {filter:
            {"limit":7}
        }

        axios.get(url_pages, {params: filter})
        .then(response => this.props.GetPages(response.data))
        .catch(error => this.props.GetPagesFailure(error))

        const url_categories = `http://startupmarket.uz/api/categories`;

        axios.get(url_categories, {params: filter})
        .then(response => this.props.GetCategories(response.data))
        .catch(error => this.props.GetCategoriesFailure(error))
        
    }

     


     renderInputField(field){
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <span className={className}>
                <h6>{field.label}</h6>
                <input 
                    className="subscribe-email-input" 
                    type="email" 
                    placeholder={strings.subscribe_email} 
                    {...field.input}
                />
            </span>
        )
    }

     handleSubmit(data)
        {
            const url = "http://startupmarket.uz/api/subscribes";

            axios.post(url, )
            .then(response => {alert("Your message has been received"); this.props.reset()} )
            .catch(error => alert(error))

        }

    render(){

      let {lang, pages, categories} = this.props; 

      strings.setLanguage(lang);

      const Menus = categories && categories.map(menu =>
            <li key = {menu.id} className  = "footer-li">
                <NavLink activeClassName = "activeClassName" className = "title-link" exact 
                to = {`/individual-category/${menu.id}`}>
                    {menu.name}
                </NavLink>
            </li> )

      

      const Pages = pages && pages.map(p => 
        <li key = {p.id} className  = "footer-li" >
            <NavLink activeClassName = "activeClassName" className = "title-link" exact to = {`/page/${p.id}`}>
                {p.title}
            </NavLink>
        </li>)

    return(
        <div className="footer-root">
        <div className="footer-container">
            <div className="footer-main">
                <div className="main-item">
                    <h6>{strings.our_company}</h6>
                        {Pages}
                         <li key = "startups" className  = "footer-li">
                            <NavLink activeClassName = "activeClassName" className = "title-link" exact to = {`/projects-all`} >
                                Startups
                            </NavLink>
                        </li>
                </div>
                <div className="main-item">
                <h6>{strings.compaign}</h6>
                    <li className = "footer-li">
                        <NavLink activeClassName = "activeClassName" className = "title-link" exact to = "create-project">
                            Start your Campaign
                        </NavLink>
                    </li>
                    
                    <li className = "footer-li">
                        <NavLink activeClassName = "activeClassName" className = "title-link" exact to = "/contacts">
                            Contacts
                        </NavLink>
                    </li>
                </div>
                <div className="main-item">
                <h6>{strings.explore}</h6>
                    {Menus}
                </div>
                <div className="main-item">
                <h6>{strings.news_letter}</h6>
                    <p>Private, secure, spam-free</p>
                    <div className="subscribe-email footer-subscribe-email">
                     <form onSubmit = {this.props.handleSubmit(this.handleSubmit)}>
                       <Field
                            name = "email"
                            placeholder = {strings.your_email}
                            label = {strings.your_email}
                            component = {this.renderInputField}
                        />
                        {/*<input className="subscribe-email-input" type="email" placeholder="Enter your email..." />*/}
                        <button className="subscribe-email-button"><i className="fa fa-paper-plane"></i></button>
                    </form>
                    </div>
                    <h6>{strings.FOLLOW_US}</h6>
                    <SocialMedia />
                </div>
            </div>
            <div className="footer-payment">
                <div className="payment-type">
                    <div className="payment" />
                    <div className="payment" />
                    <div className="payment" />
                    <div className="payment" />
                    <div className="payment" />
                </div>
                <div className="payment-amount">
                    <select>
                        <option>USD</option>
                        <option>EURO</option>
                    </select>
                     <select name="lang"  id="lang" value = {lang} onChange = {this.langChange} >
                        <option  value="en">English</option>
                        <option  value="ru">Русский</option>
                        <option value="uz">Uzbek</option>
                     </select>
                </div>
            </div>  
        </div>
    </div>

);
  }
}


function validate(values)
{
    var errors = {}

    if(!values.email)
    {
        errors.email = "Email required"
    }

    return errors;
}


const mapStateToProps = state => ({
    lang: state.lang.lang,
    pages: state.page.pages,
    categories: state.categories.categories
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({GetPages, GetPagesFailure, GetCategories, GetCategoriesFailure, ChangeLang}, dispatch)
)

export default reduxForm({
    form: "subscribe",
    validate
})(connect(mapStateToProps, mapDispatchToProps)(Footer));
