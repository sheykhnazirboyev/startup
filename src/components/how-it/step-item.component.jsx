import React from "react";
import "./step-item.styles.css";
import step from "../../img/step.png";

import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

const StepItem = (props) =>{

    let {lang} = props;

    strings.setLanguage(lang);

    return (
    <div className="step-item">
        <div className="item-icon"><img  src={step}alt = {step}  /></div>
        <div className="item-info">
            <h6 className="title">{strings.step_item_title}</h6>
            <p className="description">
                {strings.step_item_description}
              
            </p>
        </div>
    </div>
)};

const mapStateToProps = state => ({
    lang: state.lang.lang
})

export default  connect(mapStateToProps)(StepItem) ;