import React from "react";
import "./how-it.styles.css";
import StepItem from "./step-item.component";

import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})


const HowIt = (props) => {
    
    let {lang} = props;

    strings.setLanguage(lang);
    
    return(<div className="how-it-root">
        <div className="how-it-container" id = "what-is-this">

            <nav className="how-it-nav">
                <h5 className="title">{strings.how_it_title}</h5>
                <p className="subtitle">{strings.how_it_subtitle}</p>
            </nav>

            <div className="steps-container">
                <StepItem />
                <StepItem />
                <StepItem />
            </div>

        </div>
    </div>)

};

const mapStateToProps = state => ({
    lang: state.lang.lang
})

export default connect(mapStateToProps)(HowIt) ;