import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import "../categories/categories.styles.css";
import {Form } from 'react-bootstrap';


import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})


 class AllProjects extends Component {

    
    render() {
         let {lang} = this.props;

         strings.setLanguage(lang);

        console.log(this.props);
        const {projects} = this.props;
        const categoryItems = projects && projects.map(c => (
            <Link key = {c.id} to = {`individual-category/${c.id}`}>
                <div className="item"  style = {{ backgroundImage: `url("img/categories/${c.image}")`}}>
                    <div/>
                    <h4 className="title">{c.name}</h4>
                </div>
            </Link>
            ));
        return (
            <div className="categories-root">
                <div className="categories-container"  >
                    <nav className="nav">
                        <h4>{strings.categories_component_categories}</h4>
                        <div>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                                <h6>Sort by</h6>
                                <Form.Control 
                                as="select">
                                    <option default value = "ddefault">Default</option>
                                    <option key = "invest" value="invest">Invest</option>
                                </Form.Control>
                            </Form.Group>
                        </div>
                    </nav>
                    <div className="category-items">
                        {categoryItems}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    lang: state.lang.lang
})

export default connect(mapStateToProps)(AllProjects) ;
