import React, {Component} from 'react';
import "./personal-info.styles.css";
import {Link}  from 'react-router-dom';
import {connect} from 'react-redux';
import {Logged_out} from '../../redux/Auth/Auth.actions';
import {Redirect} from 'react-router-dom';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

class  PersonalInfo extends Component {
    static defaultProps = {
        user: {
            fullName: "Пол Стенли",
            projects: [],
            id: "A0092123",
            balance: 0,
        }
    }
    constructor(props)
    {
        super(props);
        this.state = {
            logOut: false
        }
    }
    handleLogOut(e)
    {
        this.props.loggedOut();
        this.setState({ logOut: true })
    }

    render()
    {
        const user = this.props.usertoken && this.props.usertoken.user;
        const {projectsCount, lang} = this.props;

        strings.setLanguage(lang);

        return (
            <div className="personal-info-root">
                <div className="personal-info-container">
                {this.state.logOut &&  <Redirect to = "/" />}
                    <div className="info-edit">
                        <img src="/img/users/avatar.png"  alt = "avatar" />
                        <div className="about">
                            <h4>{user.realm}</h4>
                            <div className="author-id">
                                <p>{strings.projects_count} :{projectsCount} </p>
                                <p>ID: {user.id}
                                </p>
                            </div>
                            <button className="blue-button logOut" onClick = {this.handleLogOut.bind(this)} >{strings.logOut}</button>
                            <button className="blue-button">{strings.edit_profile}</button>
                        </div>
                    </div>
                    <div className="info-settings">
                    <h4>{strings.balance}: 0</h4>
                        <Link to="#">{strings.balance_managment}</Link>
                        <Link to ="#">{strings.profile_settings}</Link>
                        <Link to="#">{strings.card_help}</Link>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    usertoken: state.auth.usertoken,
    lang: state.lang.lang
})

const mapDispatchToProps = dispatch => ({
    loggedOut: () => dispatch(Logged_out())
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo) ;    