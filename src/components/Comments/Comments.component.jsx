import React , {Component} from 'react';
import {connect} from 'react-redux';
import {Form } from 'react-bootstrap';
import {reduxForm, Field} from 'redux-form';
import {bindActionCreators} from 'redux';
import {CommentAction} from '../../redux/project/project.actions';
import axios from 'axios';
import moment from 'moment';
import "./Comments.styles.css";

class Comments extends Component 
{
	constructor(props) {
	    super(props);
	    this.state = {comments: [], loading: true}
	    this.handleSubmit = this.handleSubmit.bind(this);
	    this.renderComment = this.renderComment.bind(this);
	}

	handleSubmit(data){
        const url = "http://startupmarket.uz/api/comments/sendcomment";
        const userId =  this.props.usertoken && this.props.usertoken.user.id;
        const projectId = this.props.data.id;
        const newData = {...data, userId, projectId}

        axios.post(url, newData)
        .then(response =>  this.props.CommentAction(true) )
        .then(response => this.props.reset())
        .catch(error => console.log(error));

     }

     componentDidMount()
     {
     	const url = "http://startupmarket.uz/api/comments";

     	const params = {filter:{
     		where: { projectId: this.props.data.id },
     		"include":["user"],
     		"order": "id DESC",
     		limit: 10
     	}}

     	axios.get(url, {params})
     	.then(response => this.setState({ comments: response.data, loading: false }))
     	.catch(error => alert(error))
     }

     componentWillReceiveProps(newProps){
		if(newProps.commentAction !== this.props.commentAction)
		{
			const url = "http://startupmarket.uz/api/comments";

	     	const params = {filter:{
	     		where: { projectId: this.props.data.id },
	     		"include":["user"],
	     		"order": "id DESC",
	     		limit: 10
	     	}}

	     	axios.get(url, {params})
	     	.then(response => this.setState({ comments: response.data }))
	     	.then(response => this.props.CommentAction(false))
	     	.catch(error => alert(error))
		}
	}
	
     renderComment(field)
     {

		const className = `form-input ${field.meta.touched && field.meta.error ?  "has-error" : ""} `

		return(
			<div className = {className}>
				<Form.Group>
					<Form.Control 
						as = "textarea"
						className = "comment-textarea"
						rows = "2"
						placeholder = {field.placeholder}
						{...field.input}
					/>
				</Form.Group>
				<div className = "error">
					{field.meta.touched ? field.meta.error:''}
				</div>
			</div>
			)
     }



	render(){
		const {comments} = this.state;

		const allComments = comments && comments.length > 0 && comments.map(c => (
			<div key = {c.id} className = "comment-item">
				<div className = "comment-img"><img src="/img/users/avatar.png" alt=""/></div>
				<div className = "comment-body">
					<span className = "comment-author">{c.user && c.user.realm}</span>
					<span>{c.text}</span>
					</div>
				<div className = "comment-date text-muted hide-mobile">{moment(c.createdAt).fromNow()}</div>
			</div>
			))
		return(
			<div className =  "comments-root">
			<div className = "comments-container">
			{this.props.usertoken ? 
				<form onSubmit = {this.props.handleSubmit(this.handleSubmit)}  className = "row comment-form">
				<div className = " col-sm-2 comment-form-img hide-mobile">
					<img  src="/img/users/avatar.png" alt=""/>
				</div>
				<div className = "col-sm-10 mobile-textarea">
					<Field 
						name = "text"
						placeholder = "Your comment"
						component = {this.renderComment}
					/>
					<div className = "commit-buttons">
						<button className = "blue-button white hide-mobile" onClick = {this.props.reset}>
							Clear
						</button>
						<button className = "blue-button"><span className = "hide-mobile">Submit</span> <i className = "fa fa-paper-plane"></i></button>
					</div>
				</div>
				</form>
			 	: <h3>Sign up to comment</h3>
			}
			<div className = "all-commits">
				{this.state.loading ? "Loading..." : allComments}
			</div>
			</div>
			</div>
		)
	}
}


function validate(values)
{
	const errors = {}
	
	if(!values.text)
	{
		errors.text = "Comment area is empty"
	}

	return errors;
}

const mapStateToProps = state => ({
	usertoken: state.auth.usertoken,
	commentAction: state.projects.commentAction
})

const mapDispatchToProps = dispatch => (
	bindActionCreators({CommentAction}, dispatch)
)

export default reduxForm({
	validate,
	form: "Comments"
})(connect(mapStateToProps, mapDispatchToProps)(Comments))  
