import React from 'react';
import "./breadcrumb.styles.css";


 function Breadcrumb({name, path, image}) {
    
    return (
        <div className="breadcrumb-root" style = {{ backgroundImage: `url("Banner.jpg")` }} >

            <div className="breadcrumb-container">
                <div className="content">
                    <h4>{name}</h4>
                    <p>{path}</p>
                </div>
            </div>
        </div>
    )
}

export default Breadcrumb;