import React, {Component} from 'react';

import {connect} from 'react-redux';
import {ChangeLang} from '../../redux/lang/lang.actions';

import './styles.contacts.css';

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';
import info from '../../StaticPages/TopMenu.layout.json';


let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

class contacts extends Component
{
	constructor(props)
	{
		super(props);
		this.langChange = this.langChange.bind(this);
	}

	langChange(e)
	{
		this.props.changeLang(e.target.value)
	}
	
	render()
	{
		let {lang} = this.props;

        strings.setLanguage(lang);

		return(
			<div className = "root">
				<div className = "container">
					<div className = "contacts">
						<div className = "socials">
							<a href= {info.socials.facebook} className = "title-link"><i className = "fa fa-facebook-square"></i></a>
							<a href= {info.socials.instagram} className = "title-link" ><i className = "fa fa-instagram"></i></a>
							<a href= {info.socials.youtube} className = "title-link"><i className = "fa fa-youtube"></i></a>
							<a href= {info.socials.twitter} className = "title-link" ><i className = "fa fa-twitter"></i></a>
							<a href={info.socials.google} className = "title-link"><i className = "fa fa-google"></i></a>
						</div>
						<div className = "phone">
							<i className = "fa fa-phone"></i>{info.phone}
						</div>
						<div className = "email">
							<i className = "fa fa-envelope"></i>{info.email}
						</div>
						<div className = "language">
							 <select name="lang"  id="lang" value = {lang} onChange = {this.langChange} >
							 	<option  value="en">English</option>
							 	<option  value="ru">Русский</option>
							 	<option value="uz">Uzbek</option>
							 </select>
						</div>
					</div>
				</div>
			</div>
			);
	}
}

const mapStateToProps = state => ({
	lang: state.lang.lang
})

const mapDispatchToProps = dispatch => ({
	changeLang: lang => dispatch(ChangeLang(lang))
})

export default connect(mapStateToProps, mapDispatchToProps)(contacts) ;

