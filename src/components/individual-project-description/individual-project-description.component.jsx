import React, {Component} from 'react';
    
import "./individual-project-description.styles.css";

import {connect} from 'react-redux';

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';
import parse from 'html-react-parser';
import ModalComponent from '../Modal/Modal.component';

import YouTube from 'react-youtube';
import getYouTubeID from 'get-youtube-id';



let strings = new LocalizedStrings(Locale)


 class IndividualProjectDescription extends Component {
    
    constructor(props)
    {
        super(props)
        this.onReady = this.onReady.bind(this);
    }

    onReady(event) {
        // access to player in all event handlers via event.target
        event.target.pauseVideo();
      }

    render()
    {
        const opts = {
          height: '390px',
          width: '100%',
          playerVars: { // https://developers.google.com/youtube/player_parameters
            autoplay: 0
          }
        };

        var id = getYouTubeID(JSON.stringify(this.props.project.youtube_link));
        
        strings.setLanguage(this.props.lang);
        return (
            <div className  = "YouTube-video">
                { this.props.project.youtube_link ? 
                <div className="individual-project-description-root">
                    <div className="individual-project-description-container">

                        <div className="description-main-content">
                             <YouTube
                                videoId={id}
                                opts={opts}
                                onReady={this._onReady}
                              />
                            {this.props.project.details && parse(this.props.project.details) }

                        </div>
                        <div className="description-side-content">
                            <div className="help-desk">
                                <h4>{strings.card_help}</h4>
                                <p>{strings.card_become_investor}?</p>
                                <p>{strings.card_how_create}?</p>
                            </div>
                            <div className="ask-question">
                                <ModalComponent name ={strings.card_give_question} />
                            </div>

                        </div>
                    </div>
                </div>
                    : <h2>No Details</h2>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    lang: state.lang.lang
});

export default connect(mapStateToProps)(IndividualProjectDescription) ;
