import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import "./subscribe.styles.css";
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import axios from 'axios';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

class Subscribe extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            checked: true
        }
        this.onChange = this.onChange.bind(this);
        this.renderInputField = this.renderInputField.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onChange(e)
    {
        this.setState({ checked: !this.state.checked })
    }

    handleSubmit(data)
        {
            const url = "http://startupmarket.uz/api/subscribes";

            axios.post(url, data)
            .then(response => {alert("Your message has been received"); this.props.reset()} )
            .catch(error => console.log(error))

        }

     renderInputField(field){
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <span className={className}>
                <h6>{field.label}</h6>
                <input 
                    className="subscribe-email-input" 
                    type="email" 
                    placeholder={strings.subscribe_email} 
                    {...field.input}
                />
            </span>
        )
    }



    render() {

        let {lang} = this.props; 

         strings.setLanguage(lang);

        return (
            <div className="subscribe-root">
                {/* <div className="dark-background" /> */}
                <div className="subscribe-container">
                    <div className="subscribe-item">
                        <h6 className="title">{strings.subscribe_title}</h6>
                        <p className="content">
                            {strings.subscribe_content}
                        </p>
                    </div>
                    <div className="subscribe-item">
                        <div className="subscribe-email">
                            <form onSubmit = {this.props.handleSubmit(this.handleSubmit)}>
                                {/*<input 
                                    className="subscribe-email-input" 
                                    type="email" 
                                    placeholder={strings.subscribe_email} 

                                />*/}
                                <Field
                                    name = "email"
                                    placeholder = "Your email"
                                    label = "Your email"
                                    component = {this.renderInputField}
                                />
                                <button className="subscribe-email-button" type = "submit">{strings.subscribe_subscribe}</button>
                            </form>
                        </div>
                        <div className="subscribe-agreement">
                                                
                            
                        <label className="container">
                            <span>{strings.subscribe_agree} &nbsp; 
                                <Link to="#">{strings.subscribe_personal_info}</Link>
                            </span>
                            <span>{strings.subscribe_and} &nbsp; <Link to="#">
                                {strings.subscribe_subscription_terms}
                            </Link></span>
                            <input type="checkbox" checked = {this.state.checked} onChange = {this.onChange}   />
                            <span className="checkmark"></span>
                        </label>
                                
 
                            {/* <div className="helper-text">
                            <span>Я согласен на обработку <a>Персональных данных</a></span>
                            <span>А также <a>условиями подписки</a></span>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
  lang: state.lang.lang
})

function validate(values)
{
    var errors = {}

    if(!values.email)
    {
        errors.email = "Email required"
    }

    return errors;
}

export default reduxForm({
    form: "subscribe",
    validate
})(connect(mapStateToProps)(Subscribe))

