import React, {Component} from 'react';
import Slider from 'react-slick';
import Card from '../card/card.component';
import { NavLink} from 'react-router-dom';

import {connect} from 'react-redux';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

function SampleNextArrow(props) {
      const { className, style, onClick } = props;
      return (
        <div
          className={className}
          style={{ ...style, display: "none" }}
          onClick={onClick}
        />
      );
    }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "none"}}
        onClick={onClick}
      /> 
    );
  }




class TopSlider extends Component{
	
	 constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
      }

    next() {
     
        this.slider.slickNext();
      }
      previous() {
        this.slider.slickPrev();
      }


	render()
	{
   const settings = {
          dots: false,
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          adaptiveHeight: true,
         nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
        };
        
        const {projects, header, showAll, lang} = this.props;
        
              const cards = projects && projects.map(p => (
                <div key = {p.id}>{
                  p.category && p.main_image && p.user && 
                  <Card  project = {p} />
                }
                </div>
              ));
        

        strings.setLanguage(lang);
            
		return( 
			<div className = "bottomSlider">
			<div className="card-top-content-container">
	            <div className="card-top-content">
	                <h5>{header}</h5>
	                <div className="top-right">
                  {showAll &&  
                    (<NavLink exact = {true} to = "/projects-all" 
                      className="show-all" activeClassName = "activeClassName">
                        {strings.top_slider_show_all}
                      </NavLink>
                  ) }
	                

	                <div className = "slider-buttons" >
	                  <button className="button next" onClick={this.previous}>
	                    <i className = "fa fa-chevron-left"></i>
	                  </button>
	                  <button className="button prev" onClick={this.next}>
	                    <i className = "fa fa-chevron-right"></i>
	                  </button>
	                </div>

	                <div className="arrow-icon">
	                </div>
	                </div>
	            </div>
	            </div>
	             <div className="pb">
                 <div className = "container">
                    <Slider ref={c => (this.slider = c)} className="slider-container" {...settings} >
                   {cards}
                    </Slider>       
                    
                </div>
               
            </div>
			</div>
			);
	}
}

const mapStateToProps = state => ({
  lang: state.lang.lang
})

export default connect(mapStateToProps)(TopSlider) ;