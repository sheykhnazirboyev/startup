import React, {Component} from 'react';
import {Form } from 'react-bootstrap';
import "./filter.styles.css";
import {connect} from 'react-redux';
import Locale from '../../Locale/locale.json';
import LocalizedStrings from 'react-localization';


let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})


class Filter extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {"filter": "default" }
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e)
	{
		this.props.handleChange(e)
	}

	render()
	{
	
		strings.setLanguage(this.props.lang);

		return(
			<div className = "filter">
				<Form>
					<Form.Group controlId="exampleForm.ControlSelect1">
						<Form.Label className = "hide-mobile" >{strings.filter}</Form.Label>
					    <Form.Control as="select"  value = {this.props.filter} onChange = {this.handleChange} >
					      <option default value = "default">{strings.filter_default}</option>
					      <option value = "latest">{strings.filter_latest}</option>
					      <option value = "invest">{strings.filter_invest}</option>
					      <option value = "view">{strings.filter_view}</option>
					    </Form.Control>
					  </Form.Group>
				</Form>
			</div>
			);
	}
}

const mapStateToProps = state => ({
    lang: state.lang.lang
})

export default connect(mapStateToProps)(Filter) ;
