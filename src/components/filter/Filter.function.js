import orderBy from 'lodash/orderBy';

export default function Filter(items, filter)
{
	
  switch (filter) {
    case 'latest':
      return orderBy(items, 'created_date', 'desc');
    case 'invest':
      return orderBy(items, 'contact_count', 'desc');
    case 'view':
      return orderBy(items, 'view_count', 'desc');
    default:
      return items;
  }

}


