import ProjectActionTypes from "./project.action.types";


export const GetProjects = (projects) => ({
	type: ProjectActionTypes.GET_PROJECTS,
	payload: projects
});

/*INVESTMENT*/
export const GetInvestmentProjects = (projects) => ({
	type: ProjectActionTypes.GET_INVESTMENT_PROJECTS,
	payload: projects
});

/*VIEW*/
export const GetViewedProjects = (projects) => ({
	type: ProjectActionTypes.GET_VIEW_PROJECTS,
	payload: projects
});

/*LATEST*/
export const GetLatestProjects = (projects) => ({
	type: ProjectActionTypes.GET_LATEST_PROJECTS,
	payload: projects
});

/*Individual*/
export const GetIndividualProject = (project) => ({
	type: ProjectActionTypes.GET_INDIVIDUAL_PROJECT,
	payload: project
});

/*Clear individual project*/
export const ClearIndividualProject = () => (
{
	type: ProjectActionTypes.CLEAR_INDIVIDUAL_PROJECT,
	payload: null
});

/*Clear All Projects*/
export const ClearAllProjects = () => ({
	type:ProjectActionTypes.CLEAR_ALL_PROJECTS,
	payload: []
})

export const addProject = (projectDetails) =>({
    type: ProjectActionTypes.ADD_PROJECT,
    payload: projectDetails
})




export const  GetProjectsFailure = (error) => ({
	type: ProjectActionTypes.GET_PROJECTS_FAILURE,
	payload: error
});

export const SearchQuery = (query) => ({
	type: ProjectActionTypes.SEARCH_QUERY,
	payload: query
}) 

export const SearchResultAction = (result) => ({
	type: ProjectActionTypes.SEARCH_RESULT,
	payload: result
})

/* Add Comment */

export const CommentAction = (action) => ({
	type: ProjectActionTypes.COMMENT_ACTION,
	payload: action
})