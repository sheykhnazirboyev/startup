import ProjectActionTypes from "./project.action.types";

const INITIAL_STATE = {
    projects: [],
    investProjects: [],
    isInvestmentLoaded: false,
    viewProjects: [],
    isViewLoaded: false,
    latestProjects: [],
    isLatestLoaded: false,
    individual: null,
    errorMessage:null,
    isLoaded:  false,
    searchQuery: "",    
    searchResult: [],
    findResult: false,
    commentAction: false
}

const projectReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type) {
        case ProjectActionTypes.GET_PROJECTS : return {
            ...state, 
                projects: action.payload,
                isLoaded: true
            }

        case ProjectActionTypes.GET_INVESTMENT_PROJECTS : return {
            ...state, 
                investProjects: action.payload,
                isInvestmentLoaded: true
            }

          case ProjectActionTypes.GET_VIEW_PROJECTS : return {
            ...state, 
                viewProjects: action.payload,
                isViewLoaded: true
            }

        case ProjectActionTypes.GET_LATEST_PROJECTS : return {
            ...state, 
                latestProjects: action.payload,
                isLatestLoaded: true
            }


        case ProjectActionTypes.GET_INDIVIDUAL_PROJECT: return {
            ...state,
            individual: action.payload,
            isLoaded: true
        }

        case ProjectActionTypes.ADD_PROJECT:
            return {
                ...state,
                projects:[...state.projects, action.payload]
        }

        case ProjectActionTypes.CLEAR_ALL_PROJECTS: return {
            ...state,
            projects: action.payload,
            isLoaded: false
        }
        case ProjectActionTypes.CLEAR_INDIVIDUAL_PROJECT: return {
            ...state,
            individual: action.payload,
            isLoaded: false
        }
       
        case ProjectActionTypes.GET_PROJECTS_FAILURE :  return {
            ...state, 
                errorMessage: action.payload,
            }
            
        case ProjectActionTypes.SEARCH_QUERY : return {
            ...state,
            searchQuery: action.payload
        }

        case ProjectActionTypes.SEARCH_RESULT: return {
            ...state,
            searchResult: action.payload,
            findResult: true
        }

        case ProjectActionTypes.COMMENT_ACTION : return {
            ...state,
            commentAction: action.payload
        }

        default:
            return state;
    }
}

export default projectReducer;