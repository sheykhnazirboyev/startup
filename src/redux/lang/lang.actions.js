import LangTypes from './lang.action.types';

export const ChangeLang = (lang) => ({
	type: LangTypes.CHANGE_LANGUAGE,
	payload: lang
});