import LangTypes from './lang.action.types';

const INITIAL_STATE = {
	lang: "ru"
}

const langReducer = (state = INITIAL_STATE, action) => {
	switch(action.type)
	{
		case LangTypes.CHANGE_LANGUAGE: return {
			...state,
			lang: action.payload
		}

		default: return state;
	}
}

export default langReducer;
