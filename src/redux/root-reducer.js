import { combineReducers } from "redux";

import projectReducer from "./project/project.reducer";
import { reducer as formReducer } from 'redux-form';
import categories from './category/category.reducer';
import langREducer from './lang/lang.reducer';
import AuthReducer from './Auth/Auth.reducer';
import pageReducer from './page/page.reducer';

const rootReducer = combineReducers({

    projects: projectReducer,
    categories,
    lang: langREducer,
    auth: AuthReducer,
    form: formReducer,
    page: pageReducer
})

export default rootReducer;