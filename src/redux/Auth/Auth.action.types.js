const Authtypes = {
	LOGGED_IN: "LOGGED_IN",
	LOGGED_OUT: "LOGGED_OUT"
}

export default Authtypes;