import AuthTypes from './Auth.action.types'

const INITIAL_STATE = {
	usertoken: ''
}

const AuthReducer = (state = INITIAL_STATE, action) => {
	switch(action.type){
		case AuthTypes.LOGGED_IN: return {...state, usertoken: action.payload}
		case AuthTypes.LOGGED_OUT: return {...state, usertoken: ''}
		default: return  state
	}
}

export default AuthReducer;