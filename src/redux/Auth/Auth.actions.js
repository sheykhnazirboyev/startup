import AuthTypes from './Auth.action.types'

export const Logged_in = (user) => ({
	type: AuthTypes.LOGGED_IN, payload:user
})

export const Logged_out = () => ({
	type: AuthTypes.LOGGED_OUT
})


