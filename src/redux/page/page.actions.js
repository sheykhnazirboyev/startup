import PageActionTypes from './page.action.types';

export const GetPages = (pages) => ({
	type: PageActionTypes.GET_PAGES,
	payload: pages
})

export const GetPagesFailure = (error) => ({
	type: PageActionTypes.GET_PAGES_FAILURE,
	payload: error
})

export const ClearPages = () => ({
	type: PageActionTypes.CLEAR_PAGES,
	payload: []
})

export const GetIndividualPage = (page) => ({
	type: PageActionTypes.GET_INDIVIDUAL_PAGE,
	payload: page
})

export const ClearIndividualPage = () => ({
	type:PageActionTypes.CLEAR_INDIVIDUAL_PAGE,
	payload: null
})

export const GetIndividualPageFailure = (error) => ({
	type: PageActionTypes.GET_INDIVIDUAL_PAGE_FAILURE,
	payload: error
})