import PageActionTypes from "./page.action.types";

const INITIAL_STATE = {
    pages: [],
    individualPage: null,
    erorPage:null,
    erroIndividualPage: null,
    isLoading: false,
}

const pageReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type) {
        case PageActionTypes.GET_PAGES : return {
            ...state, 
                pages: action.payload,
                isLoading: true
            }

        case PageActionTypes.GET_PAGES_FAILURE : return {
            ...state, 
                errorMessage: action.payload}

        case PageActionTypes.GET_INDIVIDUAL_PAGE : return {
            ...state,
            individualPage: action.payload
        }

        case PageActionTypes.CLEAR_INDIVIDUAL_PAGE : return{
            ...state,
            individualPage: null
        }

        case PageActionTypes.CLEAR_PAGES: return {
            ...state,
            pages: []
        }
        default:
            return state;
    }
}

export default pageReducer;