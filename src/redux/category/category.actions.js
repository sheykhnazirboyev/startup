import CategoryActionTypes from './category.action.types';

export const GetCategories = (categories) => ({
	type: CategoryActionTypes.GET_CATEGORIES,
	payload: categories
});

export const GetCategoriesFailure = (error) => ({
	type: CategoryActionTypes.GET_CATEGORIES_FAILURE,
	payload: error
});

/*GetCategoriesFailure*/


