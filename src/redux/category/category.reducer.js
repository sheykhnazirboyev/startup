import CategoryActionTypes from "./category.action.types";

const INITIAL_STATE = {
    categories: [],
    errorMessage:null,
    isLoaded: false,
}

const categoryReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type) {
        case CategoryActionTypes.GET_CATEGORIES : return {
            ...state, 
                categories: action.payload,
                isLoaded: true
            }
        case CategoryActionTypes.GET_CATEGORIES_FAILURE : return {
            ...state, 
                errorMessage: action.payload}
        default:
            return state;
    }
}

export default categoryReducer;