const CategoryActionTypes = {
	GET_CATEGORIES: "GET_CATEGORIES",
	GET_CATEGORIES_FAILURE: "GET_CATEGORIES_FAILURE",
};

export default CategoryActionTypes;