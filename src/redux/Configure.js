import {createStore, applyMiddleware} from "redux";
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
 
import rootReducer from './root-reducer';
 
const persistConfig = {
  key: 'root',
  storage,
  blacklist:['projects', 'categories', 'lang', 'form', 'page']
}
 
const persistedReducer = persistReducer(persistConfig, rootReducer)
 
  let store = createStore(persistedReducer, applyMiddleware(logger))
  let persistor = persistStore(store)

let configure = { persistor: persistor, store:store}

export default configure;