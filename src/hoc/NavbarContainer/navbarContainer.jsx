import React from 'react';
import TopHeader from '../../components/contacts/component.contacts';
import MiddleHeader from '../../components/logo/component.logo';
import Lastbar from '../../components/lastbar/Lastbar.component';


 function NavbarContainer({lang}) {
    return (
        <div>
            <TopHeader lang = {lang} />
			<MiddleHeader lang = {lang} />
            <Lastbar lang = {lang} />
        </div>
    )
}

export default NavbarContainer;
