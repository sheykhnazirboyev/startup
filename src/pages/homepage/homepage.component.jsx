import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import {connect} from 'react-redux';
import axios from 'axios';
import {GetProjects, GetProjectsFailure,GetInvestmentProjects,
      GetViewedProjects, GetLatestProjects, ClearAllProjects} from '../../redux/project/project.actions';
import {GetCategories, GetCategoriesFailure} from '../../redux/category/category.actions';
import {bindActionCreators} from 'redux';

import MiddleImage from '../../components/middleImage/MiddleImage.component';
import Categories from '../../components/categories/categories.component';
import PopProjects from "../../components/pop-projects/pop-projects.component";
import HowIt from "../../components/how-it/how-it.component";
import TopSlider from '../../components/topSlider/topSlider.component';

import Subscribe from '../../components/subscribe/subscribe.component';
import Footer from '../../components/footer/footer.component';
import Partners from '../../components/partners/partners.component';
import "./homepage.styles.css";
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import BlueButton from '../../components/blue-button/blue-button.component';
import {Spinner} from 'react-bootstrap';
import Search from '../../components/Search/searchResults.component';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})



class HomePage extends Component{
 
      componentDidMount()
      {
        const url = "http://startupmarket.uz/api/projects";

          const invest_filter = {filter:{
              "include": ["category", "user"],
              "limit": 8,
              "order":"contact_count DESC"
          }}

          const view_filter = {filter:{
              "include": ["category", "user"],
              "limit": 8,
              "order":"view_count DESC"
          }}

          const latest_filter = {filter:{
              "include": ["category", "user"],
              "limit": 8,
              "order":"id DESC"
          }}

        axios.get(url, {params:invest_filter})
           .then(response => this.props.GetInvestmentProjects(response.data))
           .catch(error => this.props.GetProjectsFailure(error));

        axios.get(url, {params:latest_filter})
             .then(response => this.props.GetLatestProjects(response.data))
             .catch(error => this.props.GetProjectsFailure(error));

        axios.get(url, {params:view_filter})
             .then(response => this.props.GetViewedProjects(response.data))
             .catch(error => this.props.GetProjectsFailure(error));
             
        axios.get("http://startupmarket.uz/api/categories")
            .then(response => this.props.GetCategories(response.data))
            .catch(error => this.props.GetCategoriesFailure(error));
      }

      componentWillUnmount()
    {
      this.props.ClearAllProjects();
    }


    render()
    {
        
       const {projects,investProjects, latestProjects, viewProjects,  isCategoriesLoaded,
        categories, lang, searchQuery, isInvestmentLoaded, isViewLoaded, isLatestLoaded} = this.props;

        strings.setLanguage(lang);


        return(
            <div>
                <NavbarContainer lang = {lang} />
                {searchQuery 
                  ? <Search /> 
                  :<div>
                      <MiddleImage lang = {lang} />           
                    <div className="middle-content-root-container">
                    {isViewLoaded && viewProjects && viewProjects.length > 0 ? 
                          <TopSlider key = {1}  
                                     lang = {lang}
                                     header = {strings.top_slider_most_popular_categories} 
                                     projects = {viewProjects} 
                                     showAll = {true}
                          /> : 
                          <Spinner className = "spinner" animation="grow"  />}
                     {isCategoriesLoaded && categories &&  categories.length > 0 ? 
                                    <Categories categories = {categories} lang = {lang} /> : 
                                    <Spinner className = "spinner" animation="grow"  />}                                        
                    </div>
                    <Subscribe />
                    { isInvestmentLoaded && investProjects && investProjects.length > 0 ?
                        <PopProjects  projects = {investProjects} isLoaded = {isInvestmentLoaded} />
                        : <Spinner className = "spinner" animation="grow"  />
                    }
                    <HowIt />
                    {this.props.isLatestLoaded && latestProjects && latestProjects.length > 0 ? 
                          <span>
                            <TopSlider key = {2}  
                                       header = {strings.top_slider_most_popular_categories} 
                                       projects = {latestProjects} 
                                       showAll = {true}
                            />
                            <BlueButton text = {strings.homepage_all_projets} />
                          </span>
                          : 
                          <Spinner className = "spinner" animation="grow"  />}
                    
                    <Partners lang = {lang} />
              </div>
              }
                <Footer  lang = {lang} />
            </div>
            );
    }
}


const mapStateToProps = (state) => ({
  projects: state.projects.projects,
  isProjectsLoaded: state.projects.isLoaded,
  isInvestmentLoaded: state.projects.isInvestmentLoaded,
  isViewLoaded: state.projects.isViewLoaded,
  isLatestLoaded: state.projects.isLatestLoaded,
  categories: state.categories.categories,
  isCategoriesLoaded: state.categories.isLoaded,
  lang: state.lang.lang,
  searchQuery: state.projects.searchQuery,
  investProjects: state.projects.investProjects,
  viewProjects: state.projects.viewProjects,
  latestProjects: state.projects.latestProjects
})

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({GetProjects, GetProjectsFailure,GetCategories, GetCategoriesFailure, 
                      ClearAllProjects, GetInvestmentProjects, GetViewedProjects, GetLatestProjects}, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(HomePage) ;