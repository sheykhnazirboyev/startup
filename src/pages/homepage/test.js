import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import {connect} from 'react-redux';
import axios from 'axios';
import {GetProjects, GetProjectsFailure, ClearAllProjects} from '../../redux/project/project.actions';
import {GetCategories, GetCategoriesFailure} from '../../redux/category/category.actions';
import {bindActionCreators} from 'redux';

import MiddleImage from '../../components/middleImage/MiddleImage.component';
import Categories from '../../components/categories/categories.component';
import PopProjects from "../../components/pop-projects/pop-projects.component";
import HowIt from "../../components/how-it/how-it.component";
import TopSlider from '../../components/topSlider/topSlider.component';

import Subscribe from '../../components/subscribe/subscribe.component';
import Footer from '../../components/footer/footer.component';
import Partners from '../../components/partners/partners.component';
import "./homepage.styles.css";
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import BlueButton from '../../components/blue-button/blue-button.component';
import {Spinner} from 'react-bootstrap';

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})



class HomePage extends Component{
 
      componentDidMount()
      {
          const filter = {filter:{  
                include:["category", "user"]
          }};
          const url =  "http://startupmarket.herokuapp.com/api/projects" ;

          axios.get(url, { parms: filter})
             .then(response => this.props.GetProjects(response.data))
             .catch(error => this.props.GetProjectsFailure(error));
             
          axios.get("http://startupmarket.herokuapp.com/api/categories")
              .then(response => this.props.GetCategories(response.data))
              .catch(error => this.props.GetCategoriesFailure(error));
      }

      componentWillUnmount()
    {
      this.props.ClearAllProjects();
    }


    render()
    {
        
       const {projects, categories, lang} = this.props;

        strings.setLanguage(lang);

        return(
            <div>
                <NavbarContainer lang = {lang} />
                <MiddleImage lang = {lang} />           
                <div className="middle-content-root-container">
                {this.props.isProjectsLoaded ? 
                                        <TopSlider key = {1}  
                                                   lang = {lang}
                                                   header = {strings.top_slider_most_popular_categories} 
                                                   projects = {projects} 
                                                   showAll = {true}
                                        /> : 
                                        <Spinner className = "spinner" animation="grow"  />}
                 {this.props.isCategoriesLoaded ? 
                                        <Categories categories = {categories} lang = {lang} /> : 
                                        <Spinner className = "spinner" animation="grow"  />}                                        
                </div>
                <Subscribe />
                {/*<PopProjects  projects = {projects} isLoaded = {this.props.isProjectsLoaded} />*/}
                <HowIt />
                {this.props.isProjectsLoaded ? 
                      <>
                        <TopSlider key = {2}  
                                  
                                   header = {strings.top_slider_most_popular_categories} 
                                   projects = {projects} 
                                   showAll = {true}
                        /> : 
                        <BlueButton text = {strings.homepage_all_projets} />
                      </>
                      : 
                      <Spinner className = "spinner" animation="grow"  />}
                
                <Partners lang = {lang} />
                <Footer  lang = {lang} />
            </div>
            );
    }
}


const mapStateToProps = (state) => ({
  projects: state.projects.projects,
  isProjectsLoaded: state.projects.isLoaded,
  categories: state.categories.categories,
  isCategoriesLoaded: state.categories.isLoaded,
  lang: state.lang.lang
})

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({GetProjects, GetProjectsFailure,GetCategories, GetCategoriesFailure, ClearAllProjects}, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(HomePage) ;