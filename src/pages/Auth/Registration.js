import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import Form from 'react-bootstrap/Form';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Footer from '../../components/footer/footer.component';
import {Link} from 'react-router-dom'
import axios from 'axios';
import {connect} from 'react-redux';
import Search from '../../components/Search/searchResults.component';
import "./login.css";
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})

class Register extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			errorMessage: ""
		}

		this.renderInput = this.renderInput.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	
	handleSubmit(data)
	{
		
		const config = {
			method: "post",
			headers: {
				"Content-type": "application/json" 
			}
		}

		const url = "http://startupmarket.uz/api/users"

		axios.post(url,data, {config})
			 .then(response =>  this.props.history.push("/login"))
			 .catch(error => (error.response) 
			 	&& this.setState(
			 		{errorMessage: error.response.data && error.response.data.error && error.response.data.error.details.messages}));
	}

	renderInput(field)
	{
		const className = `form-input ${field.meta.touched && field.meta.error ? "error-form" : ""}`

		return(
			<div className = {className} >
				<Form.Group controlId={field.placeholder}>
                <h6><i className = "fa fa-check"></i> {field.label}</h6>
                <Form.Control 
                	placeholder = {field.placeholder}
                	type = {field.type}
                    {...field.input} 
                />
                <div className="error-message">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </Form.Group>
			</div>
		);
	}

	render(){
		const {handleSubmit, lang} = this.props;
		strings.setLanguage(lang);
		const {errorMessage} = this.state;
		const messages = errorMessage && 
			<div className = "error-messages">
				<span>{errorMessage.email} !</span>
				<span>{errorMessage.username} !</span>
			</div>

		return(
			<div>
				<NavbarContainer />
				{this.props.searchQuery 
                  ? <Search /> 
                  :<div className = "login-form-root">
				<div className = "login-form">
					<form onSubmit = {handleSubmit(this.handleSubmit)} >
						<div><h3>{strings.sign_up}</h3></div>
						{messages}
						<div>
							<Field 
								name = "realm" 
								type = "text" 
								label = {strings.realm} 
								placeholder = {strings.realm} 
								component = {this.renderInput} 
								/>
						</div>
						<div>
							<Field 
								name = "username" 
								type = "text" 
								label = {strings.username} 
								placeholder = {strings.username} 
								component = {this.renderInput} 
							/>
						</div>
						<div>
							<Field 
								name = "email" 
								type = "email" 
								label = {strings.your_email} 
								placeholder = {strings.your_email} 
								component = {this.renderInput} 
								/>
						</div>
						<div>
						<Field 
							name = "password" 
							type = "password" 
							label = {strings.password} 
							placeholder = {strings.password} 
							component = {this.renderInput} 
							/>
						</div>
						<div>
						<Field 
							name = "passwordVerified" 
							type = "password" 
							label = {strings.repeat_password} 
							placeholder = {strings.repeat_password} 
							component = {this.renderInput} 
						/>
						</div>
						<div className = "submit-div">
									<Link to = "/login" className = "register-link">{strings.login}</Link>
									<button type = "submit" className = "login-button">{strings.sign_up}</button>
								</div>
					</form>
				</div>
				</div>
				}
				<Footer />	
				</div>
			
			)
	}
}

const validate = (values) => {
	let errors = {};

	if(!values.realm)
	{
		errors.realm = "Full Name is empty"
	}

	if(!values.username)
	{
		errors.username = "Username is empty"
	}

	if(!values.email)
	{
		errors.email = "Email is empty"
	}


	if(values.email && (values.email.indexOf('@') < 0 || values.email.length < 7))
	{
		errors.email = "Invalid Email"
	}
	
	if(!values.password)
	{
		errors.password = "password is empty"
	}

	if(!values.passwordVerified || (values.password && ( values.passwordVerified !== values.password)))
	{
		errors.passwordVerified = "Not matched"
	}

	return errors;
}

const mapStateToProps = state => ({
	searchQuery: state.projects.searchQuery,
	lang: state.lang.lang
})

export default Register = reduxForm({
	form: "Register",
	validate
})(connect(mapStateToProps)(Register) )
