import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import Form from 'react-bootstrap/Form';
import {connect} from 'react-redux';
import axios from 'axios';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Footer from '../../components/footer/footer.component';
import {Logged_in} from '../../redux/Auth/Auth.actions';
import {Link} from 'react-router-dom';
import "./login.css";
import Search from '../../components/Search/searchResults.component';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';



let strings = new LocalizedStrings({
    ru: Locale.ru,
    en: Locale.en,
    uz: Locale.uz,
})


class Login extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			errorMessage: ""
		}

		this.renderInput = this.renderInput.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	
	handleSubmit(data)
	{
		const config = {
			method: "post",
			headers: {
				"Content-type": "application/json" 
			},
			params:{
				include:"user"
			}
		}

		const url = "http://startupmarket.uz/api/users/login"

		axios.post(url,data, config)
			 .then(response => this.props.loggedIn(response.data) )
			 .then(res => this.props.history.push("/personal-cabinet"))
			 .catch(error => this.setState({ errorMessage: "Login failed !" }))
	}


	renderInput(field)
	{
		const className = `form-input ${field.meta.touched && field.meta.error ? "error-form" : ""}`

		return(
			<div className = {className} >
				<h6><i className = "fa fa-check"></i> {field.label}</h6>
				<Form.Group controlId={field.placeholder}>
                <Form.Control 
                	placeholder = {field.placeholder}
                	type = {field.type}
                    {...field.input} 
                />
                <div className="error-message">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </Form.Group>
			</div>
		);
	}

	render(){
		const {handleSubmit, lang} = this.props;
		const {errorMessage} = this.state;
		strings.setLanguage(lang);
		return(
			<div>
				<NavbarContainer  />
				{this.props.searchQuery 
                  ? <Search /> 
                  :<div className = "login-form-root">
						<div className = "login-form">
							<form onSubmit = {handleSubmit(this.handleSubmit)} >
								<div>
									<h3>{strings.login}</h3>
								</div>
									{errorMessage && <span className = "error-messages">{errorMessage}</span>}
								<div>
									<Field 
										name = "email" 
										type = "email" 
										label = {strings.your_email}
										placeholder = {strings.your_email} 
										component = {this.renderInput} 
										/>
								</div>
								<div>
								<Field 
									name = "password" 
									type = "password" 
									label = {strings.password} 
									placeholder = {strings.password} 
									component = {this.renderInput} 
									/>
								</div>
								<div className = "submit-div">
									<Link to = "/register" className = "register-link">{strings.sign_up}</Link>
									<button type = "submit" className = "login-button small">{strings.login}</button>
								</div>
							</form>
						</div>
					</div>
				}
				<Footer />	
			</div>

			
			)
	}
}

const validate = (values) => {
	let errors = {};


	if(!values.email)
	{
		errors.email = "Email is empty"
	}


	if(values.email && (values.email.indexOf('@') < 0 || values.email.length < 7))
	{
		errors.email = "Invalid Email"
	}
	
	if(!values.password)
	{
		errors.password = "password is empty"
	}


	return errors;
}

const mapStateToProps = state => ({
	searchQuery: state.projects.searchQuery,
	lang: state.lang.lang
})

const mapDispatchToProps = dispatch => ({
	loggedIn: token => dispatch(Logged_in(token))
})

export default Login = reduxForm({
	form: "Login",
	validate
})(connect(mapStateToProps, mapDispatchToProps)(Login) )
