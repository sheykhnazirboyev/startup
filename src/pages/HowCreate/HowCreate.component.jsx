import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import Search from '../../components/Search/searchResults.component';
import {connect} from 'react-redux';
import axios from 'axios';
import parse from 'html-react-parser';
import "./howCreateProject.css";

class HowCreateProject extends Component
{

	constructor(props)
	{
		super(props)
		this.state = { page:"" }
	}
	componentDidMount()
	{	
		const url = "http://startupmarket.uz/api/pages";
		const params = {params:{filter:{where:{title:"How create project"}}}}

		//How create project
		axios.get(url, params)
		.then(response => this.setState({ page: response.data}))
		.catch(error => console.log(error));
	}		
	
	render(){
		
		const { page } = this.state;
		const body = page && page[0] && parse(page[0].body);
		
		return(
			<div>
				<NavbarContainer />
				{this.props.searchQuery 
                  ? <Search /> 
                  : <div>
						<Breadcrumb name= "How Create Project" path={`Home/How create project`} />
						<div className = "container"> 	
							<div className = "individual-category-title">
								<h2 className = "text-center">{page && page[0] && page[0].title}</h2>
							</div>
							<div className = "page-body">
								<span>{body}</span>
							</div>
						</div>
                  </div>
              	}
				<Footer />
			</div>
			)
	}
}

const mapSateToProps = state => ({
	searchQuery: state.projects.searchQuery
})

export default connect(mapSateToProps)(HowCreateProject) ;