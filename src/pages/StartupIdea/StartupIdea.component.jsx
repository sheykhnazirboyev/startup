import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Footer from '../../components/footer/footer.component';
import './startupIdea.styles.css';
import Search from '../../components/Search/searchResults.component';
import {connect} from 'react-redux';


class StartupIdea extends Component
{
	/*constructor(props)
	{
		super(props);
		this.state = {
			content: "",
			loading: true
		}
	}

	componentDidMount()
	{
		const filter = {
			filter: {"where":{"title": "About Us"}}
		}

		const url = "http://startupmarket.herokuapp.com/api/pages";

		axios.get(url, {params: filter})
		.then(response => this.setState({content: response.data[0]}))
		.catch(error => this.setState({ content :"Not Found"}));
	}
*/
	render()
	{
		
		
		return(
			<div>
			<NavbarContainer />
			{this.props.searchQuery 
              ? <Search /> 
              : <div>
					<div  className = "page-root"	>
					<div class = "page-root-child">
						<div className = "container"> 	
							<div className = "page-title">
								<h2>What is Startup Idea</h2>
							</div>
						</div>
					</div>
					</div>
              </div>
          }
				<Footer />
			</div>
			)
	}
}

const mapStateToProps = state => ({
	searchQuery: state.projects.searchQuery
})

export default connect(mapStateToProps)(StartupIdea) ;