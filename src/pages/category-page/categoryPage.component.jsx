import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import {bindActionCreators} from 'redux';
import {GetProjects,GetProjectsFailure, ClearAllProjects} from '../../redux/project/project.actions';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {Spinner} from 'react-bootstrap';
import Category  from '../../components/individual-category/individualCategory.component';
import axios from 'axios';
import "./categoryPage.styles.css";

import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';

let strings = new LocalizedStrings(Locale)


class categoryPage extends Component
{
	componentDidMount ()
	{
		const id = this.props.match.params.categoryId;
		const filter = {
			filter:{ 
						where:{categoryId: id}, 
						include:["category", "user"]
					}
		}		
		const url = "http://startupmarket.uz/api/projects"
		 axios.get(url, {
		    params: filter
		  })
		.then(response => this.props.GetProjects(response.data))
		.catch(error => this.props.GetProjectsFailure(error));

		window.scrollTo(0, 0)
	}

	componentWillReceiveProps(newProps){
		if(newProps.match.params.categoryId !== this.props.match.params.categoryId)
		{
			const id = newProps.match.params.categoryId;
			const filter = {
				filter:{ 
							where:{categoryId: id}, 
							include:["category", "user"]
						}
			}		
			const url = "http://startupmarket.herokuapp.com/api/projects"
			 axios.get(url, {
			    params: filter
			  })
			.then(response => this.props.GetProjects(response.data))
			.catch(error => this.props.GetProjectsFailure(error));
		}
		
	}

	componentWillUnmount()
	{
		this.props.ClearAllProjects();
	}

	render(){
	

		const {projects, isLoaded, errorMessage} = this.props;
		
		strings.setLanguage(this.props.lang);

		let content = '';

		if(errorMessage)
		{
			content = <Redirect  to = "/error" />

		} else	if (projects && projects.length > 0) {
			content = <div>
							{ projects[0] && projects[0].category 
								? <span>
									<Breadcrumb name = {projects[0].category.name} 
									path= {`${strings.home_page}/${projects[0].category.name}`} />
									<Category projects = {projects} category = {projects[0].category.name} />
								</span>
								: <h2 className = "text-muted text-center">Not Found </h2>
							}
					  	</div> 
		} 
		if(isLoaded && projects.length === 0)
		{
			content = <div>
						<Breadcrumb name = {strings.projects_page_empty} path= {`${strings.home_page}/${strings.projects_page_empty}`} />
						<div className = "container"> 	
							<div className = "individual-category-title">
								<h2>{strings.projects_page_no_projects}</h2>
							</div>
						</div>
				  	</div> 
		}
		
		return(
			<div className = "category-page">
			<NavbarContainer />
			<div className = "category-page-content">
				{
					isLoaded ? content
					: <Spinner className = "category-spinner" animation="grow"  />
				}
			</div>
			<Footer />	
			</div>
			)
	}
}

const mapStateToProps = state => ({
	projects: state.projects.projects,
	isLoaded: state.projects.isLoaded,
	errorMessage: state.projects.errorMessage,
	lang: state.lang.lang
})

const mapDispatchToProps = dispatch => (
	bindActionCreators({GetProjects, GetProjectsFailure,ClearAllProjects}, dispatch)
)

export default connect(mapStateToProps,mapDispatchToProps)(categoryPage) ;