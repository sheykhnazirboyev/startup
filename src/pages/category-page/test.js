import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import {bindActionCreators} from 'redux';
import {GetProjects,GetProjectsFailure, ClearAllProjects} from '../../redux/project/project.actions';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {Spinner} from 'react-bootstrap';
import Category  from '../../components/individual-category/individualCategory.component';
import axios from 'axios';
import "./categoryPage.styles.css";

class categoryPage extends Component
{
	componentDidMount ()
	{
		const id = this.props.match.params.categoryId;
		const filter = {
			filter:{ 
						where:{categoryId: id}, 
						include:["category"]
					}
		}		
		const url = "http://startupmarket.herokuapp.com/api/projects"
		 axios.get(url, {
		    params: filter
		  })
		.then(response => this.props.GetProjects(response.data))
		.catch(error => this.props.GetProjectsFailure(error));
	}

	componentWillUnmount()
	{
		this.props.ClearAllProjects();
	}

	render(){

		const {projects, isLoaded, errorMessage} = this.props;

		let content = '';

		if(errorMessage)
		{
			content = <Redirect exact to = "/error" />

		} else if (projects.length > 0 && isLoaded) {
			content =   <div>
							<Breadcrumb name = {projects[0].category.name} path= {`Home/${projects[0].category.name}`} />
							<Category projects = {projects} category = {projects[0].category.name} />
					  	</div> 
		} else if(projects.length === 0 && isLoaded){
			content = "empty"
		}

		return(
			<div className = "category-page">
			<NavbarContainer />
			<div className = "category-page-content">
				{
					isLoaded ? content
					: <Spinner className = "category-spinner" animation="grow"  />
				}
			</div>
			<Footer />	
			</div>
			)
	}
}

const mapStateToProps = state => ({
	projects: state.projects.projects,
	isLoaded: state.projects.isLoaded,
	errorMessage: state.projects.errorMessage
})

const mapDispatchToProps = dispatch => (
	bindActionCreators({GetProjects, GetProjectsFailure,ClearAllProjects}, dispatch)
)

export default connect(mapStateToProps,mapDispatchToProps)(categoryPage) ;