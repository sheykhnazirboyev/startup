import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Footer from '../../components/footer/footer.component';
import axios from 'axios';
import './AboutUs.styles.css';
import parse from 'html-react-parser'


class AboutUs extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			content: "",
			loading: true
		}
	}

	componentDidMount()
	{
		const filter = {
			filter: {"where":{"title": "About Us"}}
		}

		const url = "http://startupmarket.uz/api/pages";

		axios.get(url, {params: filter})
		.then(response => this.setState({content: response.data[0]}))
		.catch(error => this.setState({ content :"Not Found"}));
	}

	render()
	{
		let { content} = this.state
		console.log(content);
		return(
			<div>
				<NavbarContainer />
				
				<div  className = "page-root"	>
				<div class = "page-root-child">
					<div className = "container"> 	
						<div className = "about-us-title">
							<h2>About us</h2>
						</div>
						<div className = "page-body">
							<div className = "page-text">
								<div >
									{content && content.body &&  parse(content.body) }
								</div>
							</div>
							<div className = "page-img">
								<img src="newstartup.jpg" alt=""/>
							</div>
							
						</div>
					</div>
					
				</div>
				</div>
				<Footer />
			</div>
			)
	}
}

export default AboutUs;