import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import {Form} from 'react-bootstrap';
import axios from 'axios';
import {reduxForm, Field} from 'redux-form';
import Info from '../../StaticPages/Contacts.Page.json';
import {connect} from 'react-redux';
import Search from '../../components/Search/searchResults.component';
import "./Contacts.styles.css";

class ContactsPage extends Component
{	
		constructor(props)
		{
			super(props)
			this.renderInputField = this.renderInputField.bind(this);
			this.renderTextareaField = this.renderTextareaField.bind(this);
			this.handleSubmit = this.handleSubmit.bind(this);
		}
		

	 renderInputField(field){                
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        
        return (
            <div className={className}>
            <Form.Group controlId={field.placeholder}>
                <h6>{field.label}</h6>
                <Form.Control 
                    type="text" 
                    placeholder= {field.placeholder}
                    {...field.input} 
                />
                <div className="error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </Form.Group>
            </div>
        )
    }

    renderTextareaField(field){
        const className = `form-input ${field.meta.touched && field.meta.error ? 'has-error':''}`;
        return(
            <div className={className}>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                        <h6>{field.label}</h6>
                        <Form.Control 
                        as="textarea" 
                        rows="4" 
                        placeholder={field.placeholder}
                        {...field.input} />
                </Form.Group>
                 <div className="error">
                    {field.meta.touched ? field.meta.error:''}
                </div>
            </div>
        )
    }

		

		handleSubmit(data)
		{
			const url = "http://startupmarket.uz/api/feedbacks";

			axios.post(url, data)
			.then(response => {alert("Your message has been received"); this.props.reset()} )
			.catch(error => console.log(error))

		}

	render(){
		
		const {lang} = this.props;
		
		return(
			<div>
				<NavbarContainer />
				{this.props.searchQuery 
                  ? <Search /> 
                  : <div> <Breadcrumb name= "Contacts" path={`Home/Contacts`} />
				<div className = "contacts-container"> 	
					<div className = "individual-category-title">
						<h2>Contacts Page</h2>
					</div>
					<div className = "contacts-form">
						<form onSubmit = {this.props.handleSubmit(this.handleSubmit)} >
							<div>
								<Field
									name = "name"
									placeholder = "Your name"
									label = "Your name"
									component = {this.renderInputField}
								/>
							</div>
							<div>
								<Field
									name = "phone"
									placeholder = "Your phone"
									label = "Your phone"
									component = {this.renderInputField}
								/>
							</div>
							<div>
								<Field
									name = "email"
									placeholder = "Your email"
									label = "Your email"
									component = {this.renderInputField}
								/>
							</div>
							<div>
							<Field
		                        placeholder="Your message"
		                        label="Message text"
		                        name="message"
		                        component={this.renderTextareaField}
		                    />
							</div>
							<div className = "contacts-send">
								<button className = "blue-button" type = "submit">Send</button>
							</div>
						</form>
					</div>
				
					<div className = "contacts-content">
						<div className = "contact-map">
							<img src="contact.map.jpg" alt=""/>
						</div>
						<div className = "contacts-info">
							<h4>Контакты</h4>
							<p><b className = "text-muted">Режим работы</b>: {Info.work_day[`${lang}`]} </p>
							<p><b className = "text-muted">Телефон</b>: {Info.phone}</p>
							<p><b className = "text-muted">Email</b>: {Info.email}</p>
							<p><b className = "text-muted">Адрес</b>: {Info.address[`${lang}`]}</p>
							<h4>Менеджеры </h4>
							<p><b className = "text-muted">{Info.managers[0].name}</b>: {Info.managers[0].phone}</p>
							<p><b className = "text-muted">{Info.managers[1].name}</b>: {Info.managers[1].phone}</p>
						</div>
					</div>

					
				</div>
				</div>}
				<Footer />
			</div>
			)
	}
}

function validate(values)
{
	var errors = {}

	if(!values.name )
	{
		errors.name = "Required name"
	} else if(values.name.length < 3) {
		errors.name = "Invalid name"
	}

	if(!values.phone)
	{
		errors.phone = "Required phone"
	} else if(values.phone.length < 9)
	{
		errors.phone = "Invalid phone"
	}

	if(!values.email )
	{
		errors.email = "Email required"
	} else if(values.email.length < 5 || values.email.indexOf("@") < 0) {
		errors.email = "Invalid email"
	}

	if(!values.message || values.message.length < 5)
	{
		errors.message = "Message required"
	}

	return errors;
}


const mapStateToProps = state => ({
	lang: state.lang.lang,
	searchQuery: state.projects.searchQuery
})

export default reduxForm({
	form: "contact",
	validate
})(connect(mapStateToProps)(ContactsPage) )  ;