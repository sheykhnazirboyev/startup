import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import CreateProjectTips from '../../components/create-project-tips/create-project-tips.component';
import CreateProjectMain from '../../components/create-project-main/create-project-main.component';
import Footer from '../../components/footer/footer.component';
import {bindActionCreators} from 'redux';
import {connect } from 'react-redux';
import axios from 'axios';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import {GetCategories, GetCategoriesFailure} from '../../redux/category/category.actions';
import Search from '../../components/Search/searchResults.component';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';


let strings = new LocalizedStrings(Locale)


 class  CreateProjectPage extends Component{

componentDidMount()
{
    axios.get("http://startupmarket.herokuapp.com/api/categories")
    .then(response => this.props.GetCategories(response.data))
    .catch(error => this.props.GetCategoriesFailure(error));
}

render() {

        const {categories} = this.props;
        let allCategories = categories && categories;
        let {lang} = this.props;
        strings.setLanguage(this.props.lang);
    return (
        <div >
            <NavbarContainer  lang = {lang} />
            {this.props.searchQuery 
              ? <Search /> 
              : <div>
                    <Breadcrumb name= {strings.start_project} path={`${strings.home_page}/${strings.start_project}`}/>
                    <CreateProjectTips lang = {lang} />
                    <CreateProjectMain categories = {allCategories} />
                </div>
            }
             <Footer />
        </div>
    )}
}

const mapStateToProps = state => ({
    categories: state.categories.categories,
    isLoaded: state.categories.isLoaded,
    errorMessage: state.categories.errorMessage,
    lang: state.lang.lang,
    searchQuery: state.projects.searchQuery
})

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({ GetCategories, GetCategoriesFailure },dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(CreateProjectPage) ;
