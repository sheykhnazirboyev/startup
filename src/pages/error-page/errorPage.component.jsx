import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import Search from '../../components/Search/searchResults.component';
import {connect} from 'react-redux';

class errorPage extends Component
{

	
	render(){
		
		

		return(
			<div>
				<NavbarContainer />
				{this.props.searchQuery 
                  ? <Search /> 
                  : <div>
						<Breadcrumb name= "Error" path={`Home/Error`} />
						<div className = "container"> 	
							<div className = "individual-category-title">
								<h2>404 Not Found</h2>
							</div>
						</div>
                  </div>
              	}
				<Footer />
			</div>
			)
	}
}

const mapSateToProps = state => ({
	searchQuery: state.projects.searchQuery
})

export default connect(mapSateToProps)(errorPage) ;