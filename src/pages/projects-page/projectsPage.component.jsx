import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import {bindActionCreators} from 'redux';
import {GetProjects,GetProjectsFailure, ClearAllProjects} from '../../redux/project/project.actions';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {Spinner} from 'react-bootstrap';
import Category  from '../../components/individual-category/individualCategory.component';
import axios from 'axios';
import Search from '../../components/Search/searchResults.component';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';


let strings = new LocalizedStrings(Locale)


class projectsPage extends Component
{
	componentDidMount ()
	{
		const filter = {
			filter:{ 
						include:["category", "user"]
					}
		}		
		const url = "http://startupmarket.uz/api/projects"
		 axios.get(url, {
		    params: filter
		  })
		.then(response => this.props.GetProjects(response.data))
		.catch(error => this.props.GetProjectsFailure(error));

		window.scrollTo(0, 0)
	}

	componentWillUnmount()
	{
		this.props.ClearAllProjects();
	}

	handleChange(e)
	{
		this.setState({ filter: e.target.value })
	}

	

	render(){
		console.log(this.props.searchResults)
		const {projects, isLoaded, errorMessage, lang} = this.props;
		
		strings.setLanguage(lang);

		let content = '';
		
		if(errorMessage)
		{
			content = <Redirect  to = "/error" />

		} else	if (projects.length > 0) {
			content =  <div>
						{ projects[0] && projects[0].category 
							?<span>
								<Breadcrumb 
									name = {strings.projects_page_all}  
									path= {`${strings.home_page}/${strings.projects_page_projects}/${strings.projects_page_all}`} 
								/>
								<Category projects = {projects} category ={strings.projects_page_all}/>
							</span> :
							<h2 className = "text-muted text-center">Not Found</h2>
						}
					  	</div> 
		} 
		if(isLoaded && projects.length === 0)
		{
			content = <div>
							<Breadcrumb name = "Empty" 
							path= {`${strings.home_page}/${strings.projects_page_projects}/${strings.projects_page_empty}`} />
							<div className = "container"> 	
								<div className = "individual-category-title">
									<h2>{strings.projects_page_no_projects}</h2>
								</div>
							</div>
					  	</div> 
		}
		
		return(
			<div className = "category-page">
			<NavbarContainer />
			{this.props.searchQuery 
              ? <Search /> 
              :<div className = "category-page-content">
					{
						isLoaded ? content
						: <Spinner className = "category-spinner" animation="grow"  />
					}
				</div>
			}
			<Footer />	
			</div>
			)
	}
}

const mapStateToProps = state => ({
	projects: state.projects.projects,
	isLoaded: state.projects.isLoaded,
	errorMessage: state.projects.errorMessage,
	lang: state.lang.lang,
	searchQuery: state.projects.searchQuery,
	searchResults: state.projects.searchResults
})

const mapDispatchToProps = dispatch => (
	bindActionCreators({GetProjects, GetProjectsFailure,ClearAllProjects}, dispatch)
)

export default connect(mapStateToProps,mapDispatchToProps)(projectsPage) ;