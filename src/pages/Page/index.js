import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';
import Search from '../../components/Search/searchResults.component';
import {connect} from 'react-redux';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {GetIndividualPage, GetIndividualPageFailure, ClearIndividualPage} from '../../redux/page/page.actions';
import parse from 'html-react-parser';
import "./Page.styles.css";

class Page extends Component
{
	componentDidMount()
	{
		const pageId = this.props.match.params.pageId;
		const url = `http://startupmarket.uz/api/pages/${pageId}`;

		axios.get(url)
		.then(response => this.props.GetIndividualPage(response.data))
		.catch(error => this.props.GetIndividualPageFailure(error))
		
	}

	componentWillReceiveProps(newProps){
		if(newProps.match.params.pageId !== this.props.match.params.pageId)
		{
			const pageId = newProps.match.params.pageId;
			const url = `http://startupmarket.uz/api/pages/${pageId}`;

			axios.get(url)
			.then(response => this.props.GetIndividualPage(response.data))
			.catch(error => this.props.GetIndividualPageFailure(error))	
		}
	}

	 componentWillUnmount()
    {
        this.props.ClearIndividualPage();
    }

	render()
	{
		const {individualPage} = this.props;
		
		return(
			<div className = "page-root">
				<NavbarContainer />
				{this.props.searchQuery 
                  ? <Search /> 
                  : <div>
	                  <Breadcrumb name= "Page" path={`Home/Page`} />
						<div className = "page">
							<div className = "page-container">
								<div className = "page-title"> 
									<h2>{individualPage && individualPage.title}</h2>
								</div>
								<div className = "page-body">
									{individualPage &&  individualPage.body && parse(individualPage.body)}
								</div>
							</div>
						</div>
                  </div>
              }
				<Footer />
			</div>
			)
	}
}
const mapSateToProps = state => ({
	searchQuery: state.projects.searchQuery,
	individualPage: state.page.individualPage
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({GetIndividualPage, ClearIndividualPage, GetIndividualPageFailure}, dispatch)
)

export default connect(mapSateToProps, mapDispatchToProps)(Page);