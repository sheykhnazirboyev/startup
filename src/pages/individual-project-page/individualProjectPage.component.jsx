import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import IndividualProjectCard from '../../components/individual-project/individual-project.component';
import TabPanel from '../../components/tab-panel/tab-panel.component';
import IndividualProjectDescription from '../../components/individual-project-description/individual-project-description.component';
import Footer from '../../components/footer/footer.component';
import {connect} from 'react-redux';
import axios from 'axios';
import {GetIndividualProject,ClearIndividualProject, GetProjectsFailure} from '../../redux/project/project.actions';
import {bindActionCreators} from 'redux';
import {Spinner} from 'react-bootstrap';
import "./individualProjectPage.styles.css";
import Search from '../../components/Search/searchResults.component';
import LocalizedStrings from 'react-localization';
import Locale from '../../Locale/locale.json';

import Comments from '../../components/Comments/Comments.component';


let strings = new LocalizedStrings(Locale)


class  IndividualProjectPage extends Component {
    
     componentDidMount()
    {
        const id = this.props.match.params.projectId;
        const url = `http://startupmarket.uz/api/projects/view/${id}`
        
        axios.get(url)
             .then(response => this.props.GetIndividualProject(response.data))
             .catch(error => this.props.GetProjectsFailure(error));

        window.scrollTo(0, 0)
    }

    componentWillUnmount()
    {
        this.props.ClearIndividualProject();
    }

    projectTemplate = (data) => (
        
        data ? 
            <div>
                <NavbarContainer/>
                {this.props.searchQuery 
                    ? <Search /> 
                    :<div>
                            {this.props.isLoaded ? 
                            <div>
                                 <Breadcrumb name= {data.title} 
                                    path={`${strings.home_page}/${data.title}`} 
                                    image = {data && data.category.image}
                                 />
                                   {data && data.main_image && data.user  ? <IndividualProjectCard project = {data} /> : <h2>Not found</h2>}  
                                <TabPanel styled = "default">
                                <div label="Description" title = {strings.indv_projects_description} >
                                    <IndividualProjectDescription project = {data}  />  
                                </div>
                                
                                <div label={strings.indv_projects_comments} title = {strings.indv_projects_comments}>
                                    <Comments data = {data} />
                                </div>
                                </TabPanel>
                            </div>
                            : <Spinner className = "spinner" animation="grow"  />}
                    </div>
                }
                <Footer />
            </div>
        : null
    )

render(){
    strings.setLanguage(this.props.lang);
    return (
        <div>
            {this.projectTemplate(this.props.project)}
        </div>
    )}}

const mapStateToProps = (state) => ({
    project: state.projects.individual,
    isLoaded: state.projects.isLoaded,
    errorMessage: state.projects.errorMessage,
    searchQuery: state.projects.searchQuery,
    lang: state.lang.lang,
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({GetIndividualProject, ClearIndividualProject, GetProjectsFailure}, dispatch)
)

export default  connect(mapStateToProps, mapDispatchToProps)(IndividualProjectPage);