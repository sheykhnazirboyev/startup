import React, {Component} from 'react';
import "./personalCabinetPage.styles.css";

import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import PersonalInfo from '../../components/personal-info/personal-info.component';
import PersonalProjectsTab from '../../components/personal-projects-tab/personal-projects-tab.component';

import BlueButton from '../../components/blue-button/blue-button.component';
import Footer from '../../components/footer/footer.component';

import {GetProjects, GetProjectsFailure} from '../../redux/project/project.actions';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Spinner} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import axios from 'axios';
import Search from '../../components/Search/searchResults.component';


 class  PersonalCabinetPage extends Component {
    
    componentDidMount()
    {
        if(this.props.usertoken)
        {
            const userId = this.props.usertoken.user.id;
            
            const filter = {
                filter:{
                    "include": ["category", "user"], "where":{"userId": userId}
                }
            }

              axios.get("http://startupmarket.uz/api/projects", {params:filter})
                 .then(response => this.props.GetProjects(response.data))
                 .catch(error => this.props.GetProjectsFailure(error));
        }

    }

    render()
    {

        const {isProjectsLoaded, projects, usertoken} = this.props;

        const next =  usertoken ? null : <Redirect to = "/login" /> 
        const projectsCount = isProjectsLoaded && projects && projects.length;
        

        return (
            <div>
                <div className="mn-bm">
                    {next}
                    <NavbarContainer />
                    {this.props.searchQuery 
                  ? <Search /> 
                  : <div>
                        <Breadcrumb name="Личный Кабинет" path="Home/Профиль"/> 
                        {this.props.isProjectsLoaded 
                            ? 
                            <div>
                                <PersonalInfo projectsCount = {projectsCount} />
                                <PersonalProjectsTab projects = {projects} projectsCount = {projectsCount} />
                                <BlueButton text="Все проекты" />
                            </div>
                            :<Spinner className = "spinner" animation="grow"  />
                        }
                        
                      
                  </div>
                }
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    projects: state.projects.projects,
    isProjectsLoaded: state.projects.isLoaded,
    usertoken: state.auth.usertoken,
    searchQuery: state.projects.searchQuery
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({GetProjects, GetProjectsFailure}, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(PersonalCabinetPage) ;
