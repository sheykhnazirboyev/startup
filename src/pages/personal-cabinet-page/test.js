import React, {Component} from 'react';
import "./personalCabinetPage.styles.css";

import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import PersonalInfo from '../../components/personal-info/personal-info.component';
import PersonalProjectsTab from '../../components/personal-projects-tab/personal-projects-tab.component';

import BlueButton from '../../components/blue-button/blue-button.component';
import Footer from '../../components/footer/footer.component';

import {GetProjects, GetProjectsFailure} from '../../redux/project/project.actions';
import TopSlider from '../../components/topSlider/topSlider.component';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Spinner} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import axios from 'axios';


 class  PersonalCabinetPage extends Component {
    
    componentDidMount()
    {
        if(this.props.usertoken)
        {
            const userId = this.props.usertoken.user.id;
            
            const filter = {
                filter:{
                    "include": ["category", "user"], "where":{"userId": userId}
                }
            }

              axios.get("http://startupmarket.herokuapp.com/api/projects", {params:filter})
                 .then(response => this.props.GetProjects(response.data))
                 .catch(error => this.props.GetProjectsFailure(error));
        }

    }

    render()
    {

        const {isProjectsLoaded, projects, usertoken} = this.props;

        const next =  usertoken ? null : <Redirect to = "/login" /> 
        const projectsCount = isProjectsLoaded && projects && projects.length;
        
       

        return (
            <div>
                <div className="mn-bm">
                    {next}
                    <NavbarContainer />
                    <Breadcrumb name="Личный Кабинет" path="Home/Профиль"/> 
                    {this.props.isProjectsLoaded 
                        ? <PersonalInfo projectsCount = {projectsCount} />
                        :<Spinner className = "spinner" animation="grow"  />
                    }
                    <PersonalProjectsTab/>
                    {this.props.isProjectsLoaded ? 
                            <TopSlider projects = {this.props.projects} header = {false} 
                                     showAll = {false} /> : 
                            <Spinner className = "spinner" animation="grow"  />}
                    <BlueButton text="Все проекты" />
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    projects: state.projects.projects,
    isProjectsLoaded: state.projects.isLoaded,
    usertoken: state.auth.usertoken
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({GetProjects, GetProjectsFailure}, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(PersonalCabinetPage) ;
