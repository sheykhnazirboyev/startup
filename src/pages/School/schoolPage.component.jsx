import React, {Component} from 'react';
import NavbarContainer from '../../hoc/NavbarContainer/navbarContainer';
import Breadcrumb from '../../components/breadcrumb/breadcrumb.component';
import Footer from '../../components/footer/footer.component';

import YouTube from 'react-youtube';
import getYouTubeID from 'get-youtube-id';
import "./schoolPage.styles.css";
import Search from '../../components/Search/searchResults.component';
import {connect} from 'react-redux';

class SchoolPage extends Component
{

	
	 onReady(event) {
        // access to player in all event handlers via event.target
        event.target.pauseVideo();
      }

	render(){
		
		 const opts = {
          height: '100%',
          width: '100%',
          playerVars: { // https://developers.google.com/youtube/player_parameters
            autoplay: 0
          }
        };

        var id = getYouTubeID("https://www.youtube.com/watch?v=WSvREuhkoOs");
	
		return(
			<div>
				<NavbarContainer />
				{this.props.searchQuery 
                  ? <Search /> 
                  : <div>
                  	
				<Breadcrumb name= "Error" path={`Home/Error`} />
				<div className = "schoolpage-root">
				<div className = "schoolpage-container "> 	
					<div className = "individual-category-title">
						<h2>School Page</h2>
					</div>
					<div className = "school-page-content">
						<div className = "choolpage-title">
							<h2>Lesson 1: #Find an idea</h2>
						</div>
						<div className = "schoolpage-containers">
							<div className = "schoolpage-first-container">
								<div className = "school-page-body">
								<div className = "school-page-body-video">
										<YouTube
		                                videoId={id}
		                                opts={opts}
		                                onReady={this._onReady}
		                              />
									</div>
									<div className = "school-page-body-text">
									<p>
									Why do we use it?
									It is a long established fact that a reader will be distracted by the readable 
									ontent of a page when looking at its layout. The point of using Lorem Ipsum is 
									that it has a more-or-less normal distribution of letters, as opposed to using 
									'Content here, content here', making it look like readable English. Many desktop 
									publishing packages and web page editors now use Lorem Ipsum as their default model 
									ext, and a search for 'lorem ipsum' will uncover many web sites still in their 
									infancy. Various versions have evolved over the years, sometimes by accident, 
									sometimes on purpose (injected humour and the like).
									</p>
									</div>
									
								</div>
							</div>
							<div className = "schoolpage-second-container">
								
							</div>
						</div>
					</div>
				</div>
				</div>
                  </div>
              }
				<Footer />
			</div>
			)
	}
}

const mapStateToProps = state => ({
	searchQuery: state.projects.searchQuery
})

export default connect(mapStateToProps)(SchoolPage)  ;