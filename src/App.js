import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Switch,
  Route,
  HashRouter
} from "react-router-dom";


import './App.css';
import HomePage from './pages/homepage/homepage.component';
import CreateProjectPage from './pages/create-project/createProjectPage.component';
import PersonalCabinetPage from './pages/personal-cabinet-page/personalCabinetPage.component';
import IndividualProjectPage from './pages/individual-project-page/individualProjectPage.component';
import CategoryPage from './pages/category-page/categoryPage.component';
import ErrorPage from './pages/error-page/errorPage.component';
import ProjectsPage from './pages/projects-page/projectsPage.component';
import ContactsPage from './pages/Contacts/ContactsPage.component';
import SchoolPage from './pages/School/schoolPage.component';
import RegisterPage from './pages/Auth/Registration';
import LoginPage from './pages/Auth/Login';
import AboutUs from './pages/About.us/AboutUs.component';
import Page from './pages/Page';

class  App extends Component {

  
  render(){
  
    return (
      <div className="App">
      <HashRouter>
          <Switch>
            <Route exact path="/create-project" component={CreateProjectPage} ></Route>
            <Route exact path="/personal-cabinet" component={PersonalCabinetPage} ></Route>
            <Route exact path="/individual-project/:projectId" component={IndividualProjectPage} ></Route>
            <Route exact path="/individual-category/:categoryId" component={CategoryPage} ></Route>
            <Route exact path = "/projects-all" component = {ProjectsPage} ></Route>
            <Route exact path = "/contacts" component = {ContactsPage} />
            <Route exact path = "/startupmarket-school" component = {SchoolPage} />
            <Route exact path = "/login" component = {LoginPage} ></Route>
            <Route exact path = "/register" component = {RegisterPage} ></Route>
            <Route excat path = "/about-us" component = {AboutUs} />
            <Route exact path = "/page/:pageId" component = {Page} />
            <Route exact path="/" component={HomePage} ></Route>
            <Route  component={ErrorPage} ></Route>
          </Switch>
      </HashRouter>
      </div>
    );
  }
}

export default App ;
